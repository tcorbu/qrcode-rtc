#!/bin/bash

docker run -d --rm --name load_static_files -v static_files:/data alpine tail -f /dev/null
docker exec load_static_files rm  -rf /data/**
docker cp ~/webqrtc-client/data/dist  load_static_files:/data
docker exec load_static_files mv /data/dist/* /data
docker stop load_static_files
rm -rf ~/webqrtc-client
