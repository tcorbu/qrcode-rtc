declare type ShadowModes = 'open' | 'closed';

declare interface ShadowModifiers {
    mode: ShadowModes;
}

declare interface DocumentOrShadowRoot {
    readonly activeElement: Element | null;
    readonly stylesheets: StyleSheetList;

    getSelection(): Selection | null;

    elementFromPoint(x: number, y: number): Element | null;

    elementsFromPoint(x: number, y: number): Element[];
}


declare interface ShadowRoot extends DocumentOrShadowRoot, DocumentFragment {
    readonly host: Element;
    innerHTML: string;
}

declare interface HTMLElement {
    attachShadow(shadowModifiers: ShadowModifiers);

    connectedCallback(): void;

    disconnectedCallback(): void;
}

declare interface ElementDefinitionOptions {
    extends: string;
}


declare interface CustomElementRegistry {
    define(name: string, constructor: Function, options?: ElementDefinitionOptions): void;

    get(name: string): any;

    whenDefined(name: string): PromiseLike<void>;
}

declare interface Window {
    customElements: CustomElementRegistry;
}

declare interface Element {
    readonly shadowRoot: ShadowRoot;
}


interface MediaRecorderErrorEvent extends Event {
    name: string;
}

interface MediaRecorderDataAvailableEvent extends Event {
    data: any;
}

interface MediaRecorderEventMap {
    'dataavailable': MediaRecorderDataAvailableEvent;
    'error': MediaRecorderErrorEvent;
    'pause': Event;
    'resume': Event;
    'start': Event;
    'stop': Event;
    'warning': MediaRecorderErrorEvent;
}


declare class MediaRecorder extends EventTarget {

    readonly mimeType: string;
    readonly state: 'inactive' | 'recording' | 'paused';
    readonly stream: MediaStream;
    ignoreMutedMedia: boolean;
    videoBitsPerSecond: number;
    audioBitsPerSecond: number;

    ondataavailable: (event: MediaRecorderDataAvailableEvent) => void;
    onerror: (event: MediaRecorderErrorEvent) => void;
    onpause: () => void;
    onresume: () => void;
    onstart: () => void;
    onstop: () => void;

    constructor(stream: MediaStream);

    start();

    stop();

    resume();

    pause();

    isTypeSupported(type: string): boolean;

    requestData();


    addEventListener<K extends keyof MediaRecorderEventMap>(type: K, listener: (this: MediaStream, ev: MediaRecorderEventMap[K]) => any, options?: boolean | AddEventListenerOptions): void;

    removeEventListener<K extends keyof MediaRecorderEventMap>(type: K, listener: (this: MediaStream, ev: MediaRecorderEventMap[K]) => any, options?: boolean | EventListenerOptions): void;

}

// declare function require(id: string) : any;