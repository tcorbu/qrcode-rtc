declare module '@polymer/polymer' {

    class PolymerElement extends HTMLElement {
        set(path: string, value: any);
        push(path: string, value: any);
    }

    const html;

}
