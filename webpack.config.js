const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require("copy-webpack-plugin");
module.exports = {
  devtool: 'source-map',
  stats: 'normal',

  entry: {
    manifest: [
      '!file-loader?name=manifest.webmanifest!extract-loader!./src/manifest/manifest.webmanifest.js'
    ],
    presenter: [
      './src/presenter/main.ts'
    ],
    mobile: [
      './src/mobile/main.ts'
    ],
    landing: [
      './src/landing/main.ts'
    ],
    scanner: [
      './src/scanner/main.ts'
    ],
    components: [
      './src/components/main.ts'
    ],
    sw: [
      './src/service/sw.js'
    ]
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name].js',
    library: 'tcCustomComponents'
  },

  resolve: {
    extensions: ['.ts', '.js', '.scss', '.svg', '.pug']
  },
  module: {
    rules: [
      {
        test: /\.ts$/,
        loader: 'ts-loader'
      },
      {
        test: /\.md$/,
        use: [
          {
            loader: "markdown-loader",
            options: {
              /* your options here */
            }
          }
        ]
      },
      {
        test: /\.handlebars$/,
        use: [{
          loader: 'handlebars-loader', options: {}
        }],

      },
      {
        test: /\.svg$/,
        use: [

          {
            loader: 'file-loader',
            /*options: { ... }*/
          },
          {loader: 'svgo-loader'}
          //'svg-fill-loader',

        ]
      },
      {
        test: /\.jpe?g$/,
        use: [
          {
            loader: 'file-loader?name=[name].[ext]',
            /*options: { ... }*/
          }
        ]
      }
    ]
  },
  devServer: {
    https: true,
    contentBase: path.join(__dirname, 'dist'),
    before: function (app) {
      app.use(function (req, res, next) {
        // console.log(req.path) // populated!
        next();
      });
    },
    port: 58080,
    hot: true,
    inline: false

  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './src/landing/index.html.handlebars',
      inject: false,
      hash: false,
      filename: 'index.html'
    }),
    new HtmlWebpackPlugin({
      template: './src/presenter/presenter.html.handlebars',
      inject: false,
      hash: false,
      filename: 'presenter.html'
    }),
    new HtmlWebpackPlugin({
      template: './src/mobile/mobile.html.handlebars',
      inject: false,
      hash: false,
      filename: 'mobile.html'
    }),
    new HtmlWebpackPlugin({
      template: './src/scanner/scanner.html.handlebars',
      inject: false,
      hash: false,
      filename: 'scanner.html'
    }),
    new HtmlWebpackPlugin({
      template: './src/components/components.html.handlebars',
      inject: false,
      hash: false,
      filename: 'components.html'
    }),
    new CopyPlugin({
      patterns: [
        {from: "src/assets", to: "assets"},
      ],
    }),
    new webpack.ProgressPlugin({
      activeModules: false,
      entries: true,
      handler(percentage, message, ...args) {
        // custom logic
        console.log(percentage, message, args);
      },
      modules: true,
      modulesCount: 5000,
      profile: false,
      dependencies: true,
      dependenciesCount: 10000,
      percentBy: null
    })
  ]
};
