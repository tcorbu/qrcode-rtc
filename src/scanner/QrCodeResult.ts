import {BaseComponent} from "../components/BaseComponent";
import {customElement, property} from "lit-element";

@customElement('tc-qrcode-result')
export class QrCodeResult extends BaseComponent {

    @property({
        converter: (parameter: string) => {
            console.log(parameter);
        }
    })
    public result: string;

}
