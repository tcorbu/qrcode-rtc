import QrScannerLib from 'qr-scanner';
// @ts-ignore
import QrScannerWorkerPath from '!!file-loader!qr-scanner/qr-scanner-worker.min.js';
import {css, customElement, html, property, PropertyValues, query} from "lit-element";
import {Layout} from "../layout/Layout";
import {ScreenElement} from "../components/ScreenElement";

QrScannerLib.WORKER_PATH = QrScannerWorkerPath;

@customElement('tc-qr-scanner')
export class QrScanner extends ScreenElement {

  @query('video')
  video: HTMLVideoElement;
  private qrScanner: QrScannerLib;
  @property()
  private started: boolean;
  @query('tc-layout')
  layout: Layout;


  private cameraConstraints: MediaStreamConstraints = {
    video: {
      width: {
        ideal: 1024
      },
      facingMode: {ideal: 'environment'}
    }
  }
  private results: String[] = [];

  static get styles() {
    return css`

            h1 {
                display: inline-block;
            }
            div {
                position: relative;
                margin : 0 auto;
                min-height:72vh;
                overflow: hidden;
                width: 100%;
            }
            div > tc-icon {
                position : absolute;
                top: 50%;
                left : 50%;
                transform: translate(50%, 50%);
                z-index: 10;
            }
            video{
                transform : scaleX(1) translate(-50%, -50%) !important;
                position: absolute;
                min-height: 100%;
                margin : 0 auto;
                top: 50%;
                left: 50%;
            }
        `;
  }

  protected render() {
    return html`
        <tc-layout>
           <tc-icon slot="header" glyph="qrcode"></tc-icon>
           <h1 slot="header">QR Code Scanner</h1>
            <div>
                <video @click="${this.toggleStart}"></video>
                <tc-icon variant="fab" @click="${this.toggleStart}" glyph="${this.started ? '' : 'play'}">${this.started ? 'stop' : 'start'}</tc-icon>
                ${this.started ? html`` : html`<div  @click="${this.toggleStart}">Tap to start the scan</div>`}
            </div>
            <tc-list >
                ${this.results.map(item => html`<tc-qrcode-result result="${item}"></tc-qrcode-result>`)}
            </tc-list>
        </tc-layout>
    `
  }

  constructor() {
    super();

  }

  protected async firstUpdated(_changedProperties: PropertyValues) {
    super.firstUpdated(_changedProperties);
    this.qrScanner = new QrScannerLib(this.video,
      result => {
        this.showResult(result);
        this.stop();
      }
    );
    this.qrScanner.setInversionMode('both');
    // @ts-ignore
    // this.qrScanner._getCameraStream = (facingMode, exact): Promise<MediaStream> => {
    //     return navigator.mediaDevices.getUserMedia(this.cameraConstraints);
    // }
    await this.start();
  }

  private async start() {
    try {
      await this.qrScanner.start();
      this.started = true;
    } catch (e) {
      console.log('unable to start');
    }
  }

  private async stop() {
    await this.qrScanner.stop();
    this.started = false;
  }


  private toggleStart() {
    this.started ? this.stop() : this.start();

  }

  private showResult(result: string) {
    const idx = this.results.indexOf(result);
    if (~idx)
      this.results.splice(idx, 1);
    this.results.push(result);
    if (result.startsWith('https://webqrtc.com/')) {
      window.location.href = result;
    } else {
      this.layout.showNotification('Scan Successful', result, 15000);
    }
  }
}
