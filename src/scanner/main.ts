import './QrScanner';
import '../layout/Layout';
import '../layout/Icon';
import '../components/Menu';
import '../components/Footer';


import {QrScanner} from "./QrScanner";
import {assignScreenParameters} from "../labeling/assignScreenElementParameters";


const element = document.querySelector('tc-qr-scanner') as QrScanner;
assignScreenParameters(element);