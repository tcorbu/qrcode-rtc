import {VideoPatternFinderElement} from './VideoPatternFinderElement';
import {ComputerVisionService} from './ComputerVisionService';

window['customElements'].define('tc-video-pattern-finder-element', VideoPatternFinderElement);

const load = async () => {
    // const computerVisionService = new ComputerVisionService();
    // await computerVisionService.initializeWorker()
    // let videoPatternFinderElement = new VideoPatternFinderElement();
    // videoPatternFinderElement.setService(computerVisionService);
    // document.body.appendChild(videoPatternFinderElement);
};

load().then(() => {

    var host = window.document.location.host.replace(/:.*/, '');
    var ws = new WebSocket('wss://' +  host + ':8070');
    ws.onmessage = function (event) {
        console.log(event);
    };

    console.log('loaded');
});

