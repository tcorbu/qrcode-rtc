import {ProportionalBlockElement} from "./ProportionalBlockElement";
import {viewport} from "./ClientViewportHelper";
import {animationFrameTrigger} from "../async/AnimationFramePromise";
import {html} from "lit-html";
import {css, customElement} from "lit-element";


async function fitItemsToScreen(parameters: { items: any, testRatio: number, min: number, max: number, screenRatio: any }) {
    let {items, testRatio, min, max, screenRatio} = parameters;
    const rows = groupByMinRatio(items, testRatio);
    let oppositeRatio = addRowsRatio(rows);

    const arrangementCoverage = ((1 / oppositeRatio) / screenRatio) * 100;

    const blockRatio = 1 / oppositeRatio;
    if (arrangementCoverage > 125) {
        const newTestRatio = testRatio - 0.02;
        if (newTestRatio > 0) {
            const ratioResult = fitItemsToScreen({
                items: items,
                testRatio: newTestRatio,
                min: min,
                max: max,
                screenRatio: screenRatio
            });
            return ratioResult;
        }
    }
    return {rows: rows, blockRatio: blockRatio};
}


function sumRatios(bRatio: number, aRatio: number) {
    return 1 / (1 / bRatio + 1 / aRatio);
}

function groupByMinRatio(screenSizes, minRatio) {
    return screenSizes.reduce((acc, item) => {
        let row = acc.pop();
        if (row.items.length) {
            const ratio = row.ratio;
            const addedRatio = sumRatios(ratio, item.ratio);
            if (addedRatio < minRatio) {
                acc.push(row);
                row = {items: [], ratio: item.ratio};
            } else {
                row.ratio = addedRatio;
            }
        } else {
            row.ratio = item.ratio;
        }

        row.items.push(item);
        acc.push(row);
        return acc;

    }, [{items: [], ratio: 0}]);
}

function addRowsRatio(rows) {
    let oppositeRatio = 0;
    for (let row of rows) {
        oppositeRatio = oppositeRatio + row.ratio;
    }
    return 1 / oppositeRatio;
}

function absoluteSizeToProportional(reference, size) {

    return size * 100 / reference;
}

@customElement('tc-fit-ratio-layout')
export class FitRatiosLayoutElement extends ProportionalBlockElement {
    private slotElement: HTMLSlotElement;
    private observer: MutationObserver;
    private containerElement: ProportionalBlockElement;

    static get styles() {
        return css`
        :host {
            display: block;
        }
    
        .vertical {
            position: relative;
            width: 100%;
        }
    
        .slot-wrapper {
            width: 100%;
            height: 100%;
            overflow: hidden;
            display: flex;
            flex-flow: row wrap;
            align-content: center;
            justify-content: space-around;
        }
    
        .wrapper {
            position: absolute;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
    
        }
    
        .container {
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
        }`
    }

    render() {
        return html`
        <div class="vertical" style="padding-top:${this.heightProportion}%;">
            <div class="wrapper">
                <tc-proportional-block class="container">
                    <div class="slot-wrapper">
                        <slot></slot>
                    </div>
                </tc-proportional-block>
            </div>
        </div>
        `
    }

    async firstUpdated() {
        this.slotElement = this.shadowRoot.querySelector('slot') as HTMLSlotElement;
        this.containerElement = this.shadowRoot.querySelector('tc-proportional-block') as ProportionalBlockElement;
        this.observer = new MutationObserver(async (mutations: Array<MutationRecord>) => {
            await this.applyFitLayout();
        });
        this.observer.observe(this, {childList: true});

        window.addEventListener('resize', async () => {
            this.setRatioFromWindowRect();
            await this.applyFitLayout();
        });

        this.containerElement.addEventListener('ratiochange', async () => {
            this.setRatioFromWindowRect();
            await this.applyFitLayout();
        });

        this.setRatioFromWindowRect();
        await this.applyFitLayout();
    }

    private setRatioFromWindowRect() {
        const box = viewport();
        this.ratio = box.height / box.width;
    }

    disconnectedCallback() {
        this.observer.disconnect();
    }

    private async applyFitLayout() {
        await window.customElements.whenDefined('tc-proportional-block');
        await animationFrameTrigger();
        const items = Array.prototype.slice.call(this.children) as Array<ProportionalBlockElement>;

        const rect = this.getBoundingClientRect();
        const horizontalArangement = await fitItemsToScreen({
            items: items, testRatio: 1.02, min: 0, max: 1, screenRatio: rect.height / rect.width
        });
        await this.setItemsWidths(horizontalArangement, rect);
        // this.containerElement.set('ratio', horizontalArangement.blockRatio);
    }

    private async setItemsWidths(horizontalArangement, rect) {
        await animationFrameTrigger();
        const blockRatio = horizontalArangement.blockRatio;
        const containerWidth = this.computeContainerWidth(blockRatio, rect);
        for (let row of horizontalArangement.rows) {
            const rowHeight = row.ratio * containerWidth;
            let horizontalOffset = 0;
            for (let item of row.items) {
                const absoluteWidth = rowHeight * 1 / item.ratio;
                const propotionalWidth = absoluteSizeToProportional(containerWidth, absoluteWidth);

                item.style.width = `${Math.floor(propotionalWidth > 100 ? 100 : propotionalWidth) - 0.1}%`;
                horizontalOffset = horizontalOffset + absoluteWidth;
            }
        }
        this.containerElement.setAttribute('ratio', blockRatio);
        this.containerElement.style.width = `${absoluteSizeToProportional(rect.width, containerWidth)}%`;
    }


    private computeContainerWidth(blockRatio, rect) {

        const viewPortRatio = rect.height / rect.width;
        let containerWidth = rect.height * 1 / blockRatio;
        // console.log(blockRatio, viewPortRatio);
        if (blockRatio > viewPortRatio) {
            containerWidth = rect.height * 1 / blockRatio;
        } else {
            containerWidth = rect.width;
        }
        return containerWidth;
    }
}
