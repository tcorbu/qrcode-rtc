import {BaseComponent} from "../components/BaseComponent";
import {css, customElement, html, property} from "lit-element";
import {unsafeSVG} from 'lit-html/directives/unsafe-svg';
import {until} from 'lit-html/directives/until';

const usedIcons = {};

@customElement('tc-icon')
export class Icon extends BaseComponent {

    static get styles() {
        return css`
            svg {
                fill: currentColor;
            }
        `;
    }

    @property()
    public glyph: string

    public render(): unknown {
        return html``;//html`${until(this.loadIcon(this.glyph).then(res => unsafeSVG(res)), html`X`)}`;
    }

    getUsedIcons() {
        return usedIcons;
    }
    //
    // private loadIcon(glyph: string): Promise<string> {
    //     try {
    //         usedIcons[`../assets/glyphs/${glyph}.svg`] = (usedIcons[`../assets/glyphs/${glyph}.svg`] || 0) + 1;
    //         const iconPath = require(`../assets/glyphs/${glyph}.svg`).default;
    //         return glyph ? fetch(iconPath).then(res => res.text()) : Promise.resolve('');
    //     } catch (e) {
    //         console.error(e)
    //     }
    // }
}
