import {BaseComponent} from "../components/BaseComponent";
import {html} from "lit-html";
import {css, customElement, property, query} from "lit-element";
import {ToastNotification} from "../components/ToastNotification";
import "../components/GrayscaleScreen";
import {DocumentTitle} from "../components/DocumentTitle";
import {GrayscaleScreen} from "../components/GrayscaleScreen";

@customElement('tc-layout')
export class Layout extends BaseComponent {

    @property({reflect: true})
    public collapse: boolean;

    @query('.content')
    public content: HTMLDivElement;
    private documentTitle = new DocumentTitle();

    static get styles() {
        return css`
            .content {
                  display: grid;
                  grid-template-columns: auto auto auto auto;
                  grid-template-rows: minmax(64px, auto) auto minmax(32px, auto);
                  column-gap: 16px;
                    grid-template-areas: 
                            "header header header header"
                            "main main main main"
                            "footer footer footer footer";
                  transition: width 0.1s ease-in, transform 0.1s ease-out;
                  box-shadow: -4px 0px 4px grey; 
                  min-height: 100%;
                  min-width: 420px;
                  position: relative;
            }
            slot[name="header"]::slotted(*){
                display: inline;
            }
            slot[name="notification"]::slotted(*){
               max-width: 720px;
            }
            header {
              grid-area: header;
              padding: 6px;
              background : var(--background-color);
              color : var(--foreground-color);
            }
            nav {
              position: absolute;
              top: 0;
              left : 0;
              bottom: 0;
              width : 420px;
              transform: translate(-100%, 0);
              display: flex;
            }
            main {
              grid-area: main;
              display: flex;
              flex-flow: column nowrap;
            }
            footer {
              grid-area: footer;
              background : var(--background-color);
              color : var(--foreground-color);
              display: flex;
              justify-content: center;
              align-items: flex-end;
            }
            .notification-wrapper {
                position: fixed;
                bottom: 0px;
                width: 100%;
                z-index: 100;
            }
        `;
    }

    render() {
        return html`
        <style>
        ${this.collapse ? `
             .content {
                    transform: translate(420px, 0px);
                    width : calc(100% - 420px);
                  }  
            ` : `
                .content {
                
                }
            `}
        </style>
        <div class="content">
        <header>
            <tc-icon @click="${() => {
            this.collapse = !this.collapse
        }}" glyph="menu"></tc-icon>
            <slot name="header"></slot>
        </header>
        <nav>
            <slot name="navigation">
                <tc-menu></tc-menu>    
            </slot>
        </nav>
        <main>
            <slot></slot>
        </main>
        <footer>
            <slot name="footer">
                <tc-footer></tc-footer>
            </slot>
        </footer>
        </div>
        <div class="notification-wrapper">
            <slot name="notification"></slot>
        </div>`;
    }

    showNotification(title: string, content: string, autoClose: number): ToastNotification {
        this.documentTitle.restoreTitle();
        this.documentTitle.showTemporaryTitle(title, autoClose || 5000);
        const notification = new ToastNotification();
        notification.title = title;
        notification.content = content;
        notification.autoCloseTimeout = autoClose;
        notification.setAttribute('slot', 'notification')
        this.appendChild(notification);
        return notification;
    }

    async showConfirmation(title: string, element: HTMLElement, yesLabel: string, noLabel): Promise<string> {
        this.documentTitle.restoreTitle();
        this.documentTitle.showTemporaryTitle(title, 5000);
        await this.block();
        const notification = new ToastNotification();
        const result = notification.getResult();
        notification.appendChild(element);
        notification.title = title;
        notification.setAttribute('slot', 'notification')
        this.appendChild(notification);
        result.then(() => {
            this.unblock();
        })
        return result;
    }

    public async block() {
        await this.updateComplete;
        this.content.appendChild(document.createElement('tc-grayscale'));
        this.content.style.overflow = 'hidden';
    }

    public async unblock() {
        await this.updateComplete;
        const el = this.content.querySelector('tc-grayscale') as GrayscaleScreen;
        await el.close();
        this.content.style.overflow = 'auto';
    }
}
