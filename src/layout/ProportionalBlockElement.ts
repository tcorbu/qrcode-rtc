import {BaseComponent} from "../components/BaseComponent";
import {css, customElement, html, property} from "lit-element";

@customElement('tc-proportional-block')
export class ProportionalBlockElement extends BaseComponent {
    private _ratio: any;
    @property()
    public get ratio() {
        return this._ratio;
    }

    public set ratio(ratio) {
        this._ratio = ratio;
        this.handleRatioChange()
    }

    static get styles() {
        return css`
      :host {
        display: block;
    }

    .vertical {
        position: relative;
        width: 100%;
    }

    .wrapper {
        position: absolute;
        top: 0px;
        bottom: 0px;
        left: 0px;
        right: 0px;
    }

    .slot-wrapper {
        width: 100%;
        height: 100%;
        overflow: hidden;
        display: flex;
        flex-flow: row wrap;
        align-content: center;
        justify-content: space-around;
    }

    ::slotted(*) {
        width: 100%;  
        height: 100%;
    }`
    }

    @property()
    public heightProportion: number;

    protected render() {
        return html`
        <div class="vertical" style="padding-top:${this.heightProportion}%;">
            <div class="wrapper">
                <div class="slot-wrapper">
                    <slot></slot>
                </div>
            </div>
        </div>
        `
    }

    handleRatioChange() {
        this.heightProportion = this.ratio * 100;
        this.dispatchEvent(new CustomEvent('ratiochange', {bubbles: true}));
    }

}
