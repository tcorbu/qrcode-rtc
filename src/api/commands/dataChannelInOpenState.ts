export async function dataChannelInOpenState(datachannel: RTCDataChannel): Promise<void> {
    if (datachannel.readyState === "open") return Promise.resolve();
    return new Promise((res, rej) => {
        datachannel.addEventListener('open', () => res(), {once: true});
        datachannel.addEventListener('error', rej, {once: true});
        datachannel.addEventListener('close', rej, {once: true});
    });
}