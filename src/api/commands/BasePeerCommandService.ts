import {RESPONSE} from "./PeerCommands";
import {BasePeerCommand} from "./BasePeerCommand";

export class BasePeerCommandService extends EventTarget implements BasePeerCommand {

    command: string;
    sendMediator: (message) => Promise<any>;
    respondMediator: (data, messageId) => Promise<any>;
    addTrackMediator: (track: MediaStreamTrack) => Promise<any>;

    getTrackMediator: (ids: string[]) => Promise<MediaStreamTrack[]>;
    removeTrackMediator: (sender: RTCRtpSender) => Promise<void>;

    async send(message) {
        return this.sendMediator(message);
    }

    async onMessage(data: any, messageId: string, kind: number) {
        const result = await this.handleCommand(data);
        if (kind !== RESPONSE) {
            await this.respondMediator(result, messageId);
        }
    }

    activate() {
        console.log('activating');
    }

    deactivate() {
        console.log('deactivating');
    }

    handleCommand(data: any): Promise<any> {
        return Promise.resolve();
    }

    addTrack(track: MediaStreamTrack): Promise<RTCRtpSender> {
        return this.addTrackMediator(track);
    }

    getTracks(ids: string[]): Promise<MediaStreamTrack[]> {
        return this.getTrackMediator(ids);
    }

    removeTrack(sender: RTCRtpSender): Promise<void> {
        return this.removeTrackMediator(sender);
    }
}