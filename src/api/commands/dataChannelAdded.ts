export function dataChannelAdded(pc: RTCPeerConnection, id: string): Promise<RTCDataChannel> {
    return new Promise<RTCDataChannel>((res, rej) => {
        const listener = (e: RTCDataChannelEvent) => {
            if (e.channel.label === id) {
                e.channel.onopen = () => {
                    pc.removeEventListener('datachannel', listener);
                    res(e.channel);
                }
            }
        };
        pc.addEventListener('datachannel', listener);
    });
}
