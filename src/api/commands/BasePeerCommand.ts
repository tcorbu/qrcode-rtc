export interface BasePeerCommand extends EventTarget {

    command: string;
    sendMediator: (message) => Promise<any>;
    respondMediator: (data, messageId) => Promise<any>;
    addTrackMediator: (track: MediaStreamTrack) => Promise<any>
    getTrackMediator: (ids: string[]) => Promise<MediaStreamTrack[]>;
    removeTrackMediator: (sender: RTCRtpSender) => Promise<void>;

    send(message)

    onMessage(data: any, messageId: string, kind: number): Promise<any>;

    activate()

    deactivate()

    handleCommand(data: any): Promise<any>

    addTrack(track: MediaStreamTrack): Promise<RTCRtpSender>;

    getTracks(ids: string[]): Promise<MediaStreamTrack[]>;

    removeTrack(sender : RTCRtpSender): Promise<void>;

}

export declare class BasePeerCommand extends EventTarget implements BasePeerCommand {
    send(message);

    onMessage(data: any, messageId: string, kind: number): Promise<any>;

    activate();

    deactivate();

    handleCommand(data: any): Promise<any>;

    getTracks(ids: string[]): Promise<MediaStreamTrack[]>;

    removeTrack(sender : RTCRtpSender): Promise<void>;
}