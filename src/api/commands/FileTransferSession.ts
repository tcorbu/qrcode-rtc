import {FileTransferMetadata} from "../connection/MobilePeerConnection";

export class FileTransferSession {
    private seek: number;
    private arrBuffer: Uint8Array;
    private complete: boolean;
    private url: string;

    constructor(private fileMeta: FileTransferMetadata) {
        this.arrBuffer = new Uint8Array(this.fileMeta.file.size);
        this.seek = 0;
    }

    async write(data: ArrayBuffer) {
        if (this.complete) {
            throw new Error('End of file sequence was send')
        }
        try {
            if (data + '' !== this.fileMeta.endSequence) {
                this.arrBuffer.set(new Uint8Array(data), this.seek);
                this.seek = this.seek + data.byteLength;
                return false;
            } else {
                this.complete = true;
                this.downloadFile();
                return true;
            }
        } catch (err) {
            console.log('File transfer failed', err);
        }
    }

    private downloadFile() {
        const a = document.createElement('a');
        this.url = window.URL.createObjectURL(new Blob([this.arrBuffer]));
        a.href = this.url;
        a.download = this.fileMeta.file.name;
        a.type = this.fileMeta.file.type;
        a.click();
        window.URL.revokeObjectURL(this.url);
        a.remove()
    }
}
