export type SliceProgress = { blob: Blob, seek: number };

export function* sliceFilesInChunks(file: File, chunkSize: number, fileSize: number): Generator<SliceProgress> {
  let seek = 0;
  while (seek < fileSize) {
    const blob = file.slice(seek, Math.min(seek + chunkSize, fileSize));
    yield {blob: blob, seek: seek};
    seek = Math.min(seek + chunkSize, fileSize);
  }
}
