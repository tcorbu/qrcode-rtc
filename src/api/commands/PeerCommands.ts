import {
    CONNECT_TO_SIGNALING_CHANNEL,
    DISCONNECT_SIGNALING_CHANNEL,
    REQUEST_COLOR_CHANGE,
    REQUEST_TOGGLE_NO_SLEEP,
    SHARE_DEVICE_COMMAND,
    START_FILE_TRANSFER
} from "../connection/MobilePeerConnection";
import {uuid} from "short-uuid";
import {encodeMessage} from "./encodeMessage";
import {dataChannelAdded} from "./dataChannelAdded";
import {sliceFilesInChunks} from "./sliceFileInChunks";
import {parseMessage} from "./parseMessage";
import {FileTransferSession} from "./FileTransferSession";
import {ENUMERATE_DEVICE_COMMAND, OK} from "../connection/PresenterPeerConnection";
import {TrackSenderMapping} from "../connection/PeerConnection";
import * as NoSleep from 'nosleep.js';
import {PeerCommandRegistry} from "./PeerCommandRegistry";
import {dataChannelInOpenState} from "./dataChannelInOpenState";

export interface GeoLocation {
    accuracy: number;
    altitude: number;
    altitudeAccuracy: number;
    heading: number;
    latitude: number;
    longitude: number;
    speed: number;

}

interface PeerMessage {
    type: string;
}

export interface GeoLocationResponseMessage extends PeerMessage {
    position: GeoLocation;
}

export type ProgressCallback = (current: number, total: number) => void;
// @ts-ignore
const noSleep = new NoSleep();


export const DISCONNECTED = 'disconnected';

export const CONNECTED = 'connected';
export const COMMAND_RECEIVED = 'command_received';
export const RESPONSE = 1;
export const REQUEST = 0;

export interface MessageHandler extends EventTarget {
    sendCommand(command: { data: any; command: string }): Promise<any>;

    respond(command: string, data: any, messageId: string): void;

    ready(): Promise<void>;

    addTrack(track: MediaStreamTrack): RTCRtpSender;

    getTracks(ids: string[]): Promise<any>;

    removeTrack(sender: RTCRtpSender): Promise<void>;
}

interface TrackReceiverMapping {
    receiver: RTCRtpReceiver;
    streams: ReadonlyArray<MediaStream>;
    track: MediaStreamTrack;
    transceiver: RTCRtpTransceiver;
}

export class PeerCommands extends EventTarget implements MessageHandler {

    private receivedTracks = new Map<string, TrackReceiverMapping>();

    constructor(private commandChannel: RTCDataChannel, private pc: RTCPeerConnection, deviceId: string, private registry: PeerCommandRegistry) {

        super();
        registry.messageHandler = this;

        this.pc.addEventListener('track', (ev: RTCTrackEvent) => {
            this.receivedTracks.set(ev.track.id, {
                receiver: ev.receiver,
                streams: ev.streams,
                track: ev.track,
                transceiver: ev.transceiver
            });
            const customEvent = new CustomEvent('track_' + ev.track.id, {detail: this.receivedTracks.get(ev.track.id)});
            ev.track.addEventListener("ended", () => {
                const item = this.receivedTracks.get(ev.track.id);
                this.receivedTracks.delete(ev.track.id);
            });
            ev.track.addEventListener("mute", () => {
                const item = this.receivedTracks.get(ev.track.id);
                this.receivedTracks.delete(ev.track.id);
            });
            ev.track.addEventListener("isolationchange", () => {
                const item = this.receivedTracks.get(ev.track.id);
                this.receivedTracks.delete(ev.track.id);
            });
            this.dispatchEvent(customEvent);
        });

        this.commandChannel.addEventListener('message', async (e: MessageEvent<string>) => {
            const {message, messageId, kind} = parseMessage(e);
            switch (message.type) {
                case  ENUMERATE_DEVICE_COMMAND:
                    await this.handleEnumerateDeviceCapabilities(message, messageId);
                    break;
                case  SHARE_DEVICE_COMMAND:
                    await this.handleCastMediaDeviceCommand(message, messageId);
                    break;
                case  REQUEST_TOGGLE_NO_SLEEP:
                    await this.handleToggleNoSleep(message, messageId);
                    break;
                case  REQUEST_COLOR_CHANGE:
                    await this.handleRequestColorChange(message, messageId);
                    break;
                case START_FILE_TRANSFER:
                    await this.startFileTransfer(message, messageId);
                    break;
                default:
                    if (kind === RESPONSE) {
                        this.dispatchEvent(new CustomEvent(messageId, {
                            detail: {message, messageId, kind}
                        }));
                        break;
                    } else {
                        this.dispatchEvent(new CustomEvent(message.command, {
                            detail: {message, messageId, kind}
                        }));
                        break;
                    }
            }
        });
    }

    getTracks(ids: string[]): Promise<MediaStreamTrack[]> {
        const promises = [];
        for (const id of ids) {
            if (this.receivedTracks.get(id)) {
                promises.push(Promise.resolve(this.receivedTracks.get(id)));
            } else {
                promises.push(new Promise(res => this.addEventListener('track_' + id, (e: CustomEvent) => {
                    res(e.detail.track);
                }, {once: true})));
            }
        }
        return Promise.all(promises);
    }

    async removeTrack(sender: RTCRtpSender): Promise<void> {
        return this.pc.removeTrack(sender);
    }

    async respond(command, data, messageId): Promise<void> {
        await this.ready();
        this.commandChannel.send(encodeMessage(RESPONSE, messageId, {command, data}))
    }

    public ready(): Promise<void> {
        return dataChannelInOpenState(this.commandChannel);
    }

    private async handleEnumerateDeviceCapabilities(message, messageId) {
        const mediaDeviceInfos = await navigator.mediaDevices.enumerateDevices();
        const responseMessage = {
            type: 'deviceEnumerationResponse',
            devices: mediaDeviceInfos.filter(info => info.kind === 'videoinput')
        };
        this.commandChannel.send(encodeMessage(RESPONSE, messageId, responseMessage));
    }

    private async handleCastMediaDeviceCommand(message, messageId) {
        await this.addLocalVideoStream(message.deviceId);
        this.commandChannel.send(encodeMessage(RESPONSE, messageId, {type: OK}));
    }

    private async startFileTransfer(message, messageId) {
        const fileTransferSession = new FileTransferSession(message);
        // open data channel
        const channel = this.pc.createDataChannel(messageId);
        await new Promise((res, rej) => channel.onopen = res)
        channel.onmessage = async (e) => {
            const done = await fileTransferSession.write(e.data)
            if (done) {
                this.commandChannel.send(encodeMessage(RESPONSE, messageId, {type: OK}));
                channel.close();
            }
        }
    }

    async requestMediaDeviceStream(deviceId: string) {
        await this.sendCommand({type: SHARE_DEVICE_COMMAND, deviceId: deviceId});
    }

    async sendDisconnectForSignalingChannelCommand(deviceId: string) {

        console.log('sendDisconnectForSignalingChannelCommand', deviceId);
        const message = {type: DISCONNECT_SIGNALING_CHANNEL, deviceId: deviceId};
        return await this.sendCommand(message);
    }

    async sendConnectToSignalingChannelCommand(deviceId: string) {

        console.log('sendConnectToSignalingChannelCommand', deviceId)
        const message = {type: CONNECT_TO_SIGNALING_CHANNEL, deviceId: deviceId};
        const response = await this.sendCommand(message);
        console.log('got response', response);
    }

    async sendCommand<T>(message, timeout = 5000): Promise<T> {
        await this.ready();
        return new Promise((res) => {
            const messageId = uuid();
            this.addEventListener(messageId, (event: CustomEvent) => {
                res(event.detail?.message?.data);
            }, {once: true});
            this.commandChannel.send(encodeMessage(REQUEST, messageId, message));
        });
    }

    public async sendFile(file: File, progressCallback: ProgressCallback) {
        const fileSize = file.size;
        const chunkSize = 64000; // bytes
        const id = uuid();
        const message = {
            type: START_FILE_TRANSFER,
            file: {name: file.name, type: file.type, size: file.size},
            endSequence: id
        };
        this.commandChannel.send(encodeMessage(REQUEST, id, message));
        const channel = await dataChannelAdded(this.pc, id);
        channel.binaryType = 'arraybuffer';
        const blobs = sliceFilesInChunks(file, chunkSize, fileSize);
        for (const {blob, seek} of blobs) {
            channel.send(new Uint8Array(await blob.arrayBuffer()));
            await new Promise((res) => channel.addEventListener('bufferedamountlow', res, {once: true}));
            progressCallback(seek, fileSize);
        }
        channel.send(id);
    }

    private async addLocalVideoStream(deviceId: string) {
        try {
            const currentStream = await navigator.mediaDevices.getUserMedia({
                video: {deviceId: deviceId},
                audio: true
            });
            await this.addStream(currentStream);
        } catch (e) {
            console.error('unable to get stream', e);
        }
    }

    // call addStream() anytime on either end to add camera and microphone to connection
    addStream(stream: MediaStream): TrackSenderMapping[] {
        const senders = [];
        try {
            for (const track of stream.getTracks()) {
                senders.push({sender: this.pc.addTrack(track, stream), track: MediaStreamTrack});
            }
        } catch (err) {
            console.error(err);
        }
        return senders;
    }

    addTrack(track: MediaStreamTrack): RTCRtpSender {
        return this.pc.addTrack(track);
    }

    async requestPeerMediaDevice(constraints: MediaStreamConstraints) {
        const trackIds = await this.sendCommand({command: 'getUserMedia', constraints});
        return new Promise(() => {
            let ids;
            this.pc.addEventListener('track', () => {

            });
        })

    }

    close() {
        this.commandChannel.close();
        this.commandChannel = null;
    }

    async requestToggleNoSleep() {
        await this.ready();
        await this.sendCommand({type: REQUEST_TOGGLE_NO_SLEEP});
    }

    async requestParameters() {
        await this.ready();
        await this.sendCommand({type: REQUEST_TOGGLE_NO_SLEEP});
    }

    private async handleToggleNoSleep(message: any, messageId: any) {
        console.log('handleToggleNoSleep');
        if (noSleep.isEnabled) {
            noSleep.disable()
        } else {
            await noSleep.enable();
        }
        this.commandChannel.send(encodeMessage(RESPONSE, messageId, {type: OK, enabled: noSleep.isEnabled}));
    }

    async requestColorChange(color: string) {
        return await this.sendCommand({type: REQUEST_COLOR_CHANGE, color: color});

    }

    private async handleRequestColorChange(message: any, messageId: any) {
        this.dispatchEvent(new CustomEvent('change-color', {detail: message}));
    }
}
