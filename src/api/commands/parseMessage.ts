export function parseMessage(e: MessageEvent<string>) {
    let data = e.data;
    let messageId, kind;
    const indexOf = data.indexOf('|');
    const parts = data.split('|', 3);

    if (parts.length === 3) {
        messageId = parts[0];
        kind = parseInt(parts[1], 10);
        data = parts[2];
    }
    const message = JSON.parse(data);
    return {message, messageId, kind};
}
