import {BaseComponent} from "../../components/BaseComponent";
import {RESPONSE} from "./PeerCommands";
import {BasePeerCommand} from "./BasePeerCommand";

export class BasePeerCommandComponent extends BaseComponent implements BasePeerCommand {

    public command: string;
    public sendMediator: (message) => Promise<any>;
    public respondMediator: (data, messageId) => Promise<any>;
    public addTrackMediator: (track: MediaStreamTrack) => Promise<RTCRtpSender>;
    public getTrackMediator: (ids: string[]) => Promise<MediaStreamTrack[]>;
    public removeTrackMediator: (sender: RTCRtpSender) => Promise<void>;

    public async send(message) {
        return this.sendMediator(message);
    }

    async onMessage(data: any, messageId: string, kind: number) {

        const result = await this.handleCommand(data);
        if (kind !== RESPONSE) {
            await this.respondMediator(result, messageId);
        }

    }

    activate() {
        console.log('activating');
    }

    deactivate() {
        console.log('deactivating');
    }

    public handleCommand(data: any): Promise<any> {
        return Promise.resolve();
    }

    public addTrack(track: MediaStreamTrack): Promise<RTCRtpSender> {
        return this.addTrackMediator(track);
    }

    async getTracks(ids: string[]): Promise<MediaStreamTrack[]> {
        return this.getTrackMediator(ids);
    }

    removeTrack(sender: RTCRtpSender): Promise<void> {
        return this.removeTrackMediator(sender);
    }
}