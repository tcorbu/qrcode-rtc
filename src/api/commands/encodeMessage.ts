export function encodeMessage(kind: number, messageId: string, message) {
    return messageId + '|' + kind + '|' + JSON.stringify(message);

}
