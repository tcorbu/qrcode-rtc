import {customElement, html, LitElement} from 'lit-element';

@customElement('tc-file-system-manager')
export class FileSystemManager extends LitElement {


    public render() {
        return html`<button @click="${this.handleWriteButtonClick}">Write Temporary file</button><button @click="${this.handleReadButtonClick}">Read Temporary File</button>`
    }

    writeFileInit(fs) {
        fs.root.getFile('log.txt', {create: true}, this.wireFileEntry.bind(this), this.errorHandler.bind(this));
    }


    readFileInit(fs) {
        fs.root.getFile('log.txt', {create: true}, this.readFileEntry.bind(this), this.errorHandler.bind(this));
    }

    wireFileEntry(fileEntry) {
        // Create a FileWriter object for our FileSystemFileEntry (log.txt).
        fileEntry.createWriter(function (fileWriter) {
            fileWriter.onwriteend = function (e) {
                console.log('Write completed.', e);
            };

            fileWriter.onerror = function (e) {
                console.log('Write failed: ' + e.toString());
            };
            const blob = new Blob(['body { color: red; }'], {type: 'text/css'});
            fileWriter.seek(fileWriter.length);
            fileWriter.write(blob, true);

            console.log(fileWriter);
        }, this.errorHandler.bind(this));

    };


    readFileEntry(fileEntry) {

        // Get a File object representing the file,
        // then use FileReader to read its contents.
        fileEntry.file(function (file) {
            const reader = new FileReader();

            reader.onloadend = function (e) {
                console.log(e.target.result)
            };


            reader.readAsText(file);
        }, this.errorHandler.bind(this));


    };

    async handleWriteButtonClick() {
        // @ts-ignore
        window.webkitRequestFileSystem(window.TEMPORARY, 1024 * 1024, this.writeFileInit.bind(this), this.errorHandler.bind(this));
        // @ts-ignore

    }

    errorHandler() {

    }

    async handleReadButtonClick() {
        //@ts-ignore
        window.webkitRequestFileSystem(window.TEMPORARY, 1024 * 1024, this.readFileInit.bind(this), this.errorHandler.bind(this));
    };
}
