import {injectable} from "tsyringe";
import {CONNECTED, DISCONNECTED, MessageHandler} from "./PeerCommands";
import {Deferred} from "../../async/Deferred";
import {uuid} from "short-uuid";
import {PeerConnection} from "../connection/PeerConnection";
import {BasePeerCommand} from "./BasePeerCommand";

interface RegistryEntry {
    componentFactory: typeof BasePeerCommand;
}

interface RegistryMapping {
    [command: string]: RegistryEntry;
}

interface PeerCommandMessageDetail {
    message: any;
    messageId: string;
    kind: number;
}

@injectable()
export class PeerCommandRegistry {
    private registry: RegistryMapping = {}
    public readonly instanceId = uuid();
    private peerConnectionAvailable: Deferred<PeerConnection> = new Deferred<PeerConnection>();

    public set messageHandler(messageHandler: MessageHandler) {
        this.messageHandlerAvailable.resolve(messageHandler);
    }

    public set peerConnection(peerConnection: PeerConnection) {
        this.peerConnectionAvailable.resolve(peerConnection);
    }

    private messageHandlerAvailable: Deferred<MessageHandler> = new Deferred<MessageHandler>();

    registerPeerCommand(command: string, componentFactory: typeof BasePeerCommand) {
        this.registry[command] = {componentFactory: componentFactory};
    }

    resolve(command: string): BasePeerCommand {
        const component = new this.registry[command].componentFactory;
        component.sendMediator = (async (data) => {
            const peerCommands = await this.messageHandlerAvailable.promise;
            return peerCommands.sendCommand({command: command, data: data});
        });

        component.respondMediator = (async (data, messageId) => {
            const peerCommands = await this.messageHandlerAvailable.promise;
            return peerCommands.respond(command, data, messageId);
        });

        component.addTrackMediator = async (mediaStream: MediaStreamTrack) => {
            const peerCommands = await this.messageHandlerAvailable.promise;
            return peerCommands.addTrack(mediaStream);
        }

        component.getTrackMediator = async (ids: string[]) => {
            const peerCommands = await this.messageHandlerAvailable.promise;
            return peerCommands.getTracks(ids);
        }
        component.removeTrackMediator = async (sender: RTCRtpSender) => {
            const peerCommands = await this.messageHandlerAvailable.promise;
            return peerCommands.removeTrack(sender);
        }


        this.messageHandlerAvailable.promise.then((peerCommands) => {
            component.activate();
            peerCommands.addEventListener(DISCONNECTED, () => {
                component.deactivate();
            });
            peerCommands.addEventListener(CONNECTED, () => {
                component.activate();
            });
            peerCommands.addEventListener(command, (e: CustomEvent<PeerCommandMessageDetail>) => {
                component.onMessage(e.detail.message.data, e.detail.messageId, e.detail.kind);
            });

            peerCommands.addEventListener('track', (e) => {
                console.log(e);
            });
        });

        return component;
    }

    listCommands(): Array<string> {
        return Object.keys(this.registry);
    }
}