import {BaseComponent} from "../../components/BaseComponent";
import {customElement} from "lit-element";
import {ANSWER_EVENT, DESCRIPTION_EVENT, ICE_CANDIDATE_EVENT, PING, PONG, START_EVENT} from "./SSESignalingChannel";

export interface SignalingChannel extends HTMLElement {

    connect(deviceId: string);

    postStartSignaling(localDeviceId: string, remoteDeviceId: string)

    postCandidate(remoteId: string, candidate: RTCIceCandidate);

    postDescription(remoteId: string, offer: RTCSessionDescriptionInit)

    postPing(remoteId: string);

    postPong(deviceId: string);

    close();
}

@customElement('tc-local-signaling-channel')
export class LocalSignalingChannel extends BaseComponent implements SignalingChannel {

    private deviceId: string;

    public peerSignalingChannel: SignalingChannel

    constructor() {
        super();
    }

    async connect(deviceId: string): Promise<void> {
        // console.log('connecting to signaling channel');
        //
        this.deviceId = deviceId;
        // this.sse = new EventSource(`${this.signalingEndpoint}/listen/${deviceId}`);
        this.addEventListener('message', async (e: CustomEvent) => {
            const message = JSON.parse(e.detail);
            console.log('incoming message', message);
            switch (message.data?.type) {
                case DESCRIPTION_EVENT:
                    this.dispatchEvent(new CustomEvent(DESCRIPTION_EVENT, {detail: JSON.parse(message.data.data)}));
                    break;
                // case ANSWER_EVENT:
                //     this.dispatchEvent(new CustomEvent(ANSWER_EVENT, {detail: JSON.parse(message.data.data)}));
                //     break;
                case ICE_CANDIDATE_EVENT:
                    this.dispatchEvent(new CustomEvent(ICE_CANDIDATE_EVENT, {detail: JSON.parse(message.data.data)}));
                    break;
            }
        });

        this.addEventListener(PING, async (e: CustomEvent) => {
            console.log('received ping');
            const message = JSON.parse(e.detail);
            await this.postPong(null);
        });
        return Promise.resolve();
    }


    async postStartSignaling(localDeviceId: string, remoteDeviceId: string) {
        // console.log('Posting start', localDeviceId, remoteDeviceId);
        // this.peerSignalingChannel.dispatchEvent(new CustomEvent('start', {detail: JSON.stringify({'id': localDeviceId})}))
    }


    async postCandidate(remoteId: string, candidate: RTCIceCandidate) {
        this.peerSignalingChannel.dispatchEvent(new CustomEvent(ICE_CANDIDATE_EVENT, {detail: candidate}))
    }

    async postDescription(remoteId: string, offer: RTCSessionDescriptionInit) {
        this.peerSignalingChannel.dispatchEvent(new CustomEvent(DESCRIPTION_EVENT, {detail: offer}))
    }

    async postPing(remoteId: string) {
        // console.log('Posting Description', remoteId);
        const pongPromise = new Promise<void>((res, rej) => {
            const listener = async (e: MessageEvent) => {
                this.removeEventListener(PONG, listener)
                res();
            };
            this.addEventListener(PONG, listener);
        })

        this.peerSignalingChannel.dispatchEvent(new CustomEvent(PING));
        await pongPromise;
    }

    close() {

        // console.log('Closing signaling channel');
        // this.sse.close();
        // this.sse = null;
        // this.isOpened = false;
    }

    public async postPong(deviceId: any) {
        this.peerSignalingChannel.dispatchEvent(new CustomEvent(PONG, {detail: deviceId}));
        this.dispatchEvent(new CustomEvent(START_EVENT, {detail: deviceId}));
    }

}