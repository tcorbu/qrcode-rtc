import {property} from "lit-element";
import {BaseComponent} from "../../components/BaseComponent";
import {SignalingChannel} from "./LocalSignalingChannel";

export const DESCRIPTION_EVENT = 'offer';
export const ANSWER_EVENT = 'answer';
export const ICE_CANDIDATE_EVENT = 'iceCandidate';
export const START_EVENT = 'start';
export const PONG_EVENT = 'pong';
export const PING = 'ping';
export const PONG = 'pong';

export class SSESignalingChannel extends BaseComponent implements SignalingChannel {

    private sse: EventSource;
    private deviceId: string;
    isOpened: boolean;
    @property()
    signalingEndpoint: string = `https://webqrtc.com/api/v1/signaling`;

    async connect(deviceId: string): Promise<void> {
        console.log('connecting to signaling channel');

        this.deviceId = deviceId;
        this.sse = new EventSource(`${this.signalingEndpoint}/listen/${deviceId}`);

        this.sse.addEventListener('message', async (e) => {
            const message = JSON.parse(e.data);
            console.log('incoming message', message);
            switch (message.data?.type) {
                case DESCRIPTION_EVENT:
                    this.dispatchEvent(new CustomEvent(DESCRIPTION_EVENT, {detail: JSON.parse(message.data.data)}));
                    break;
                case ANSWER_EVENT:
                    this.dispatchEvent(new CustomEvent(ANSWER_EVENT, {detail: JSON.parse(message.data.data)}));
                    break;
                case ICE_CANDIDATE_EVENT:
                    this.dispatchEvent(new CustomEvent(ICE_CANDIDATE_EVENT, {detail: JSON.parse(message.data.data)}));
                    break;
            }
        });

        this.sse.addEventListener(PING, async (e: MessageEvent) => {
            console.log('recived ping')
            const message = JSON.parse(e.data);
            await this.postPong(message.data.deviceId);
        });

        return new Promise((res, rej) => {
            this.sse.addEventListener('open', () => {
                this.isOpened = true;
                res();
            }, {once: true});
            this.sse.addEventListener('error', () => {
                this.isOpened = false;
                rej()
            }, {once: true});
        });
    }


    async postStartSignaling(localDeviceId: string, remoteDeviceId: string) {
        console.log('Posting start', localDeviceId, remoteDeviceId);
        const formData = new FormData();
        formData.set('type', 'start');
        formData.set('data', JSON.stringify({'id': localDeviceId}));
        await fetch(`${this.signalingEndpoint}/send-event/${remoteDeviceId}`, {
            method: 'POST',
            body: formData
        });
    }


    async postCandidate(remoteId: string, candidate: RTCIceCandidate) {
        console.log('Posting candidate', remoteId, candidate);
        const form = JSON.stringify(candidate);
        const formData = new FormData();
        formData.set('data', form);
        formData.set('type', ICE_CANDIDATE_EVENT);
        await fetch(`${this.signalingEndpoint}/send-event/${remoteId}`, {
            method: 'POST',
            body: formData
        });
    }

    async postDescription(remoteId: string, offer: RTCSessionDescriptionInit) {
        console.log('Posting Description', remoteId, offer);
        const form = JSON.stringify(offer);
        const formData = new FormData();
        formData.set('data', form);
        formData.set('type', DESCRIPTION_EVENT);
        await fetch(`${this.signalingEndpoint}/send-event/${remoteId}`, {
            method: 'POST',
            body: formData
        });
    }

    async postPing(remoteId: string) {

        console.log('Posting Description', remoteId);
        const pongPromise = new Promise<void>((res, rej) => {
            const listener = async (e: MessageEvent) => {
                const message = JSON.parse(e.data);
                if (message.data.deviceId === remoteId) {
                    this.sse.removeEventListener(PONG, listener)
                    console.log('recived pong')
                    res();
                    this.dispatchEvent(new CustomEvent(PONG_EVENT, {detail: message}));
                }
            };
            this.sse.addEventListener(PONG, listener);

        })

        const formData = new FormData();
        formData.set('type', PING);
        formData.set('deviceId', this.deviceId);
        await fetch(`${this.signalingEndpoint}/send-event/${remoteId}/ping`, {
            method: 'POST',
            body: formData
        });
        await pongPromise;
    }

    close() {
        if (this.sse && this.sse.readyState !== EventSource.CLOSED) {
            console.log('Closing signaling channel');
            this.sse.close();
            this.sse = null;
            this.isOpened = false;
        }
    }

    public async postPong(deviceId: any) {
        console.log('Posting Pong', deviceId);
        const formData = new FormData();
        formData.set('type', PONG);
        formData.set('deviceId', this.deviceId);
        await fetch(`${this.signalingEndpoint}/send-event/${deviceId}/pong`, {
            method: 'POST',
            body: formData
        });
        this.dispatchEvent(new CustomEvent(START_EVENT, {detail: deviceId}));
    }


}

window.customElements.define('tc-signaling-channel', SSESignalingChannel);
