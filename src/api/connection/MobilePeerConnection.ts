import {PeerConnection} from "./PeerConnection";

import {PeerCommands} from "../commands/PeerCommands";
import {inject, injectable} from "tsyringe";
import {SignalingChannel} from "../signaling/LocalSignalingChannel";
import {customElement} from "lit-element";
import {PeerCommandRegistry} from "../commands/PeerCommandRegistry";

export const ready = 'ready';
export const enumerateDevicesCommand = 'enumarate devices';
export const deviceEnumerationResponse = 'deviceEnumerationResponse';
export const SHARE_DEVICE_COMMAND = 'shareDeviceCommand';
export const REQUEST_TOGGLE_NO_SLEEP = 'REQUEST_TOGGLE_NO_SLEEP';
export const REQUEST_LOCATION = 'REQUEST_LOCATION';
export const REQUEST_COLOR_CHANGE = 'REQUEST_COLOR_CHANGE';
export const START_FILE_TRANSFER = 'START_FILE_TRANSFER';
export const DISCONNECT_SIGNALING_CHANNEL = 'DISCONNECT_SIGNALING_CHANNEL';
export const CONNECT_TO_SIGNALING_CHANNEL = 'CONNECT_TO_SIGNALING_CHANNEL';

export interface FileTransferMetadata {
    type: string,
    file: { name: string, type: string, size: number },
    endSequence: string
}

const COMMAND = "command";

@injectable()
@customElement('tc-mobile-peer-connection')
export class MobilePeerConnection extends PeerConnection {


    constructor(@inject('mobileSignalingChannel') protected signalingChannel: SignalingChannel,
                @inject(PeerCommandRegistry) public readonly registry: PeerCommandRegistry) {
        super(false, signalingChannel);
    }

    async retrievePeerCommands(): Promise<PeerCommands> {
        if (!this.peerCommands) {
            await this.initializeCommandDataChannel();
        }
        await this.peerCommands.ready();
        return this.peerCommands;
    }

    private initializeCommandDataChannel() {
        const remoteCommandChannel = this.pc.createDataChannel(COMMAND);
        this.peerCommands = new PeerCommands(remoteCommandChannel, this.pc, this.deviceId, this.registry);
        this.peerCommands.ready().then(() => this.peerCommandsAvailable.resolve(this.peerCommands));
    }

}

