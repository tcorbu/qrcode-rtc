import {LitElement, property} from "lit-element";
import {SignalingChannel} from "../signaling/LocalSignalingChannel";
import {DESCRIPTION_EVENT, ICE_CANDIDATE_EVENT} from "../signaling/SSESignalingChannel";
import {PeerCommands} from "../commands/PeerCommands";
import {Deferred} from "../../async/Deferred";
import {PeerCommandRegistry} from "../commands/PeerCommandRegistry";

export type TrackSenderMapping = { sender: RTCRtpSender, track: MediaStreamTrack };
export const DISCONNECTED_EVENT = 'disconnected';
export const CONNECTED_EVENT = 'connected';
export const STATS_EVENT = 'stats-generator-created';
export const RENEGOTIATING_EVENT = 'renegotiation';

/**
 * A perfect signaling negotiation pattern that establishes a connection that allows to be updated with streams or data channels
 * uses polite/impolite logic to ignore or comply
 * based on https://w3c.github.io/webrtc-pc/#perfect-negotiation-example
 * TODO: disconnect from the signaling channel when it is not needed anymore
 */
export abstract class PeerConnection extends LitElement {

    protected peerCommands: PeerCommands;
    protected peerCommandsAvailable = new Deferred<PeerCommands>();
    private currentStream: MediaStream;

    @property()
    protected remoteId: string;

    protected _deviceId: string;

    @property()
    private configuration = {
        'iceServers': [{'urls': 'stun:stun.l.google.com:19302'}],
    };
    protected sendAnswer = true;

    protected pc: RTCPeerConnection;

    // - The perfect negotiation logic

    // keep track of some negotiation state to prevent races and errors
    private makingOffer = false;
    public readonly registry: PeerCommandRegistry;

    @property()
    public set deviceId(deviceId) {
        this._deviceId = deviceId;
        if (deviceId) {
            console.log('connecting to the signaling channel')
            this.signalingChannel.close();
            this.signalingChannel.connect(deviceId);
        }
    };

    public get deviceId() {
        return this._deviceId;
    }

    constructor(private polite: boolean, protected signalingChannel: SignalingChannel) {
        super();
        this.signalingChannel.addEventListener(ICE_CANDIDATE_EVENT, this.iceCandidateListener);
        this.signalingChannel.addEventListener(DESCRIPTION_EVENT, this.descriptionListener);
    }

    public start(remoteId: string) {
        this.pc = new RTCPeerConnection(this.configuration);
        this.remoteId = remoteId;
        // send any ice candidates to the other peer
        this.pc.onicecandidate = async ({candidate}) => await this.signalingChannel.postCandidate(remoteId, candidate);

        // let the "negotiationneeded" event trigger offer generation
        this.pc.onnegotiationneeded = async () => {
            this.dispatchEvent(new CustomEvent(RENEGOTIATING_EVENT, {detail: this.pc.connectionState}));
            try {
                this.makingOffer = true;
                const offer = await this.pc.createOffer();
                if (this.pc.signalingState != "stable") return;
                await this.pc.setLocalDescription(offer);
                await this.signalDescription(remoteId, this.pc.localDescription)
            } catch (e) {
                console.log(`onnegotiationneeded failed ${e}`);
            } finally {
                this.makingOffer = false;
            }
        };

        this.pc.onconnectionstatechange = async () => {
            console.log(this.pc.connectionState);

            if (this.pc.connectionState === 'connected') {
                this.dispatchEvent(new CustomEvent(CONNECTED_EVENT));

                while (true) {
                    const stats = await this.pc.getStats();
                    this.dispatchEvent(new CustomEvent(STATS_EVENT, {detail: stats}));
                    if (this.pc.connectionState !== 'connected') {
                        break;
                    }
                    await new Promise((res) => {
                        setTimeout(res, 1000);
                    });
                }
            } else {
                if (['failed', 'closed'].includes(this.pc.connectionState)) {
                    this.cleanUp();
                }
                this.dispatchEvent(new CustomEvent(DISCONNECTED_EVENT, {detail: this.pc.connectionState}));
            }
        }
    }

    protected async signalDescription(remoteId: string, desc) {
        await this.signalingChannel.postDescription(remoteId, desc);
    }

    private iceCandidateListener: (e: CustomEvent) => Promise<void> = (async (e: CustomEvent) => {
        console.log('received ice candidate', e.detail);
        if (e.detail) {
            await this.pc.addIceCandidate(new RTCIceCandidate(e.detail));
        }
    }).bind(this);

    private descriptionListener: (e: CustomEvent) => Promise<void> = (async (e: CustomEvent) => {

        const description = e.detail;
        // An offer may come in while we are busy processing SRD(answer).
        // In this case, we will be in "stable" by the time the offer is processed
        // so it is safe to chain it on our Operations Chain now.
        const offerCollision = description.type == "offer" &&
            (this.makingOffer || this.pc.signalingState != "stable");
        const ignoreOffer = !this.polite && offerCollision;
        if (ignoreOffer) {
            return;
        }

        if (offerCollision) {
            await Promise.all([
                this.pc.setLocalDescription({type: "rollback"}).catch((e) => {
                    console.log('Unable to rollback local description, ignoring', e);
                }),
                this.pc.setRemoteDescription(description)
            ]);
        } else {
            await this.pc.setRemoteDescription(description);
        }
        if (description.type == "offer") {
            await this.pc.setLocalDescription(await this.pc.createAnswer());
            await this.signalDescription(this.remoteId, this.pc.localDescription)
        }
    }).bind(this);

    protected createRenderRoot(): Element | ShadowRoot {
        return this;
    }

    abstract retrievePeerCommands(): Promise<PeerCommands>;

    async startSignaling(remoteId: string) {
        for (let i = 0; i < 3; i++) {
            const res = await Promise.race([this.signalingChannel.postPing(remoteId), new Promise(res => setTimeout(() => res('timeout'), 1000))]);
            if (res !== 'timeout') {
                break;
            }
        }

        await this.start(remoteId);
    }

    public cleanUp() {
        this.pc.getSenders().forEach(sender => this.pc.removeTrack(sender));
        this.peerCommands.close();
        this.peerCommands = null;

        if (this.currentStream) {
            const tracks = this.currentStream.getTracks();
            tracks.forEach(track => this.currentStream.removeTrack(track));
        }
        this.pc.close();
        this.pc = null;
        this.peerCommandsAvailable = new Deferred<PeerCommands>();
    }


    removeStream(entries: TrackSenderMapping[]) {
        for (const entry of entries) {
            this.pc.removeTrack(entry.sender);
        }
    }
}
