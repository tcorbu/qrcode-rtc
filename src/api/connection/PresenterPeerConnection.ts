import {PeerConnection} from "./PeerConnection";
import {START_EVENT} from "../signaling/SSESignalingChannel";
import {PeerCommands} from "../commands/PeerCommands";
import {inject, injectable} from "tsyringe";
import {customElement} from "lit-element";
import {PeerCommandRegistry} from "../commands/PeerCommandRegistry";

export const READY = 'ready';
export const ENUMERATE_DEVICE_COMMAND = 'enumarate devices';
export const CAST_MEDIA_DEVICE_COMMAND = 'shareDeviceCommand';
export const OK = 'OK';

@injectable()
@customElement('tc-presenter-peer-connection')
export class PresenterPeerConnection extends PeerConnection {


    private startEventListener: (e: CustomEvent) => Promise<void> = (async (e: CustomEvent) => {
        console.log('starting connection', e.detail);
        await this.start(e.detail);
    }).bind(this);

    constructor(@inject('presenterSignalingChannel') protected signalingChannel, @inject(PeerCommandRegistry) public readonly registry: PeerCommandRegistry) {
        super(true, signalingChannel);
        this.signalingChannel.addEventListener(START_EVENT, this.startEventListener);
    }


    public async start(remoteId: string) {
        super.start(remoteId);
        this.pc.addEventListener('datachannel', async (e) => {
            console.log('incoming data channel');
            if (e.channel.label === 'command') {
                await new Promise((res) => e.channel.onopen = res);
                this.handleCommandChannel(e.channel);
            }
        });
        this.pc.onconnectionstatechange = () => {
            console.log(this.pc.connectionState);
            if (['failed', 'closed'].includes(this.pc.connectionState)) {
                this.cleanUp();
            }
        }
    }

    private handleCommandChannel(dataChannel: RTCDataChannel) {
        this.peerCommands = new PeerCommands(dataChannel, this.pc, this.deviceId, this.registry);
        this.peerCommandsAvailable.resolve(this.peerCommands);
    }

    retrievePeerCommands(): Promise<PeerCommands> {
        return this.peerCommandsAvailable.promise;
    }
}
