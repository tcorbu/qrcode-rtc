self['importScripts']('/lib/opencv.js.txt');

const cv = self['cv'];
const respond = self['postMessage'] as Function;

function findTemplate(src, templ) {
    let dst = new cv.Mat();
    let mask = new cv.Mat();
    cv.matchTemplate(src, templ, dst, cv.TM_CCOEFF, mask);
    // cv.normalize(dst, dst, -1000, 1000, cv.NORM_MINMAX);
    let result = cv.minMaxLoc(dst, mask);
    // console.log(result, mask);
    let maxPoint = result.maxLoc;
    //let color = new cv.Scalar(255, 0, 0, 255);
    //let point = new cv.Point(maxPoint.x + templ.cols, maxPoint.y + templ.rows);
    // cv.rectangle(dst, maxPoint, point, color, 2, cv.LINE_8, 0);

    const imageDataResult = new ImageData(
        new Uint8ClampedArray(dst.data, dst.cols, dst.rows), dst.cols, dst.rows
    );
    const match = {x: maxPoint.x + 32, y: maxPoint.y + 32, match: result.minVal, result: imageDataResult};
    console.log(match);
    // cv.rectangle(src, maxPoint, point, color, 2, cv.LINE_8, 0);
    // cv.imshow(this.findCanvas, src);
    //
    templ.delete();
    dst.delete();
    mask.delete();
    return match;

}

// Setup the termination criteria, either 10 iteration or move by atleast 1 pt
const termCrit = new cv.TermCriteria(cv.TERM_CRITERIA_EPS | cv.TERM_CRITERIA_COUNT, 10, 1);
//
// let video = document.getElementById('videoInput');
// let cap = new cv.VideoCapture(video);
function prepareTrackerCamshift(frame, marker) {
    // take first frame of the video
    // let frame = new cv.Mat(video.height, video.width, cv.CV_8UC4);
    // cap.read(frame);

    // hardcode the initial location of window
    let trackWindow = new cv.Rect(marker.location.x, marker.location.y, marker.location.w, marker.location.h);

    // set up the ROI for tracking
    let roi = frame.roi(trackWindow);
    let hsvRoi = new cv.Mat();
    cv.cvtColor(roi, hsvRoi, cv.COLOR_RGBA2RGB);
    cv.cvtColor(hsvRoi, hsvRoi, cv.COLOR_RGB2HSV);
    let mask = new cv.Mat();
    let lowScalar = new cv.Scalar(30, 30, 0);
    let highScalar = new cv.Scalar(180, 180, 180);
    let low = new cv.Mat(hsvRoi.rows, hsvRoi.cols, hsvRoi.type(), lowScalar);
    let high = new cv.Mat(hsvRoi.rows, hsvRoi.cols, hsvRoi.type(), highScalar);
    cv.inRange(hsvRoi, low, high, mask);
    let roiHist = new cv.Mat();
    let hsvRoiVec = new cv.MatVector();
    hsvRoiVec.push_back(hsvRoi);
    cv.calcHist(hsvRoiVec, [0], mask, roiHist, [180], [0, 180]);
    cv.normalize(roiHist, roiHist, 0, 255, cv.NORM_MINMAX);

    // delete useless mats.
    roi.delete();
    hsvRoi.delete();
    mask.delete();
    low.delete();
    high.delete();
    hsvRoiVec.delete();

    const hsv = new cv.Mat(frame.rows, frame.cols, cv.CV_8UC3);
    let hsvVec = new cv.MatVector();
    hsvVec.push_back(hsv);

    return {roiHist, hsvVec}
}

function processVideo(frame, marker) {
    try {
        let dst = new cv.Mat();
        const hsv = new cv.Mat(frame.rows, frame.cols, cv.CV_8UC3);
        cv.cvtColor(frame, hsv, cv.COLOR_RGBA2RGB);
        cv.cvtColor(hsv, hsv, cv.COLOR_RGB2HSV);
        cv.calcBackProject(marker.hsvVec, [0], marker.roiHist, dst, [0, 180], 1);

        // apply camshift to get the new location
        [marker.trackBox, marker.trackWindow] = cv.CamShift(dst, marker.trackWindow, termCrit);

        // Draw it on image
        let pts = cv.rotatedRectPoints(marker.trackBox);
        // cv.line(frame, pts[0], pts[1], [255, 0, 0, 255], 3);
        // cv.line(frame, pts[1], pts[2], [255, 0, 0, 255], 3);
        // cv.line(frame, pts[2], pts[3], [255, 0, 0, 255], 3);
        // cv.line(frame, pts[3], pts[0], [255, 0, 0, 255], 3);
        // cv.imshow('canvasOutput', frame);

        // schedule the next one.
        // let delay = 1000/FPS - (Date.now() - begin);
        // setTimeout(processVideo, delay);
        dst.delete();
    } catch (err) {
        // utils.printError(err);
    }
};

// schedule the first one.
setTimeout(processVideo, 0);

self.onmessage = function (message) {
    const data = message.data;
    if (data.operation === 'findMarkers') {
        const res = [];
        const source = cv.matFromImageData(data.frame);
        for (const marker of data.markers) {
            const template = cv.matFromImageData(marker.template);
            const result = findTemplate(source, template);
            marker.match = result;
        }
        source.delete();

        respond(data.markers);
    } else {
        setTimeout(() => {
            respond('operation not known')
        }, 1000);
    }
};
