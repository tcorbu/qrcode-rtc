import {customElement, html} from "lit-element";
import {BasePeerCommandComponent} from "../api/commands/BasePeerCommandComponent";
import {GeoLocation} from "../api/commands/PeerCommands";
import {BasePeerCommandService} from "../api/commands/BasePeerCommandService";

export class PresenterRemoteLocation extends BasePeerCommandService {


    protected render(): unknown {
        return html`

        `;
    }

    async handleCommand(data: any): Promise<any> {
        const position: GeolocationPosition = await new Promise((res, rej) => navigator.geolocation.getCurrentPosition(res, rej, {enableHighAccuracy: true}));
        const result: GeoLocation = {
            accuracy: position.coords.accuracy,
            altitude: position.coords.altitude,
            altitudeAccuracy: position.coords.altitudeAccuracy,
            heading: position.coords.heading,
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            speed: position.coords.speed,
        }
        return result;
    }

}