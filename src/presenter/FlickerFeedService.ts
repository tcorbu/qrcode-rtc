import {Deferred} from "../async/Deferred";
import {raceAganinstTimeout} from "../async/raceAgainstTimeout";
import {uuid} from "short-uuid";

export class FlickerFeedService {

    static async get(...tags: Array<string>) {
        const scriptElement = document.createElement('script');

        const callbackName = FlickerFeedService.generateCallbackName();
        const url = FlickerFeedService.createUrl(tags, callbackName);
        scriptElement.setAttribute('src', url);
        document.body.appendChild(scriptElement);
        const d = new Deferred<any>();
        window[callbackName] = (data) => {
            d.resolve(data);
            document.body.removeChild(scriptElement);
            delete window[callbackName];
        };
        return raceAganinstTimeout(d, 2000);

    }


    private static createUrl(tags: string[], callbackName) {
        return `https://api.flickr.com/services/feeds/photos_public.gne?jsoncallback=${callbackName}&tags=${tags.join(',')}&format=json&accuracy=16`
    }

    private static generateCallbackName() {
        const id = uuid();
        return `jsonp_${id}`
    }
}
