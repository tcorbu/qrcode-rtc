import {injectable} from "tsyringe";

@injectable()
export class PixabayImageSearchService {

  async findSearchQuery(query: string) {

    return fetch('https://webqrtc.com/api/v1/bus/graphql', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
      body: JSON.stringify({
        query: `
        {
          pixbay {
            image(query: "${query}", editors_choice: true, safesearch: true) {
              hits {
                largeImageURL
              }
            }
          }
        }

        `
      })
    })
      .then(r => r.json());


  }
}
