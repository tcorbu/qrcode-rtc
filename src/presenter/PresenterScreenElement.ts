import {ProportionalBlockElement} from "../layout/ProportionalBlockElement";
import {animationFrameTrigger} from "../async/AnimationFramePromise";
import {PresenterPeerConnection} from "../api/connection/PresenterPeerConnection";
import {css, customElement, html, property, PropertyValues, query} from "lit-element";
import {ScreenElement} from "../components/ScreenElement";
import {Layout} from "../layout/Layout";
import {inject, injectable} from "tsyringe";
import * as Color from "color";
import {StringToColorNamer} from "../labeling/StringToColorNamer";
import {applyColors} from "../labeling/assignScreenElementParameters";
import {PeerCommandRegistry} from "../api/commands/PeerCommandRegistry";
import {BasePeerCommand} from "../api/commands/BasePeerCommand";
import {PixabayImageSearchService} from "./PixabayImageSearchService";
import "../components/QrCodeElement";

@injectable()
@customElement('tc-presenter')
export class PresenterScreenElement extends ScreenElement {
  static componentStyle = css`

    img {
        width: 100%;
    }

    .image-placeholder {
        background-size: cover;
    }

    .background {
      display: grid;
      grid-template-areas: "tl t tr"
                           "l c r"
                           "bl b br";
      grid-gap: .5rem;
      align-items: flex-center;
            justify-content: center;
      flex: 1;
      color: var(--foreground-color);
      background-color: var(--background-color);
    }

    .join-container {
        max-width: 480px;
        padding: 8px;
        grid-area: c;
    }

    h2 {
      grid-area: t;
    }

    .join-container tc-proportional-block {
        width: 360px;
    }
    `;

  @property()
  localDevice: string;
  @property()
  roomUrl: string;


  @query('.content')
  private layout: Layout;
  public registry: PeerCommandRegistry;
  private peerServices: BasePeerCommand[] = [];

  public static get styles() {
    return [PresenterScreenElement.componentStyle]
  }

  render() {
    return html`
        <style>
            .background {
              background-image: url(${this.backgroundImage});
              background-size: cover;
              background-position: center;
            }
        </style>


            <div class="background content">
              <h2 slot="header">${this.name}</h2>
              <div class="join-container">
              <tc-proportional-block ratio="1">
                ${this.deviceId && this.color ?
      html`
                        <a href="${this.buildUrl()}">
                            <tc-qrcode data="${this.buildUrl()}" backgroundcolor="${this.backgroundColor}" foregroundcolor="${this.foregroundColor}"></tc-qrcode>
                        </a>

                ` :
      html`
                   <tc-loading></tc-loading>
                `}
                </tc-proportional-block>
              </div>
            </div>
        `;
  }

  public readonly type: string = 'presenter';

  constructor(@inject(PixabayImageSearchService) private image: PixabayImageSearchService, @inject(PresenterPeerConnection)  peerConnection: PresenterPeerConnection) {
    super();

    this.peerConnection = peerConnection as PresenterPeerConnection;
    this.registry = this.peerConnection.registry;
  }

  async connectedCallback() {
    super.connectedCallback();
    const result = await this.image.findSearchQuery('hello');
    console.log(result);
    const commands = await this.peerConnection.retrievePeerCommands();
    commands.addEventListener('change-color', async (evt: CustomEvent) => {
      console.log(evt.detail);
      const color = new Color(evt.detail.color);
      const result = await StringToColorNamer.computeForColor(color)
      await applyColors(result, this);
    })
    console.log(commands);
  }

  // private resolveRoomId() {
  //     const url = new URL(window.location.toString());
  //     const urlRoomID = url['searchParams'].get('roomId');
  //     return urlRoomID || uuid();
  // }


  private resolveMobilePath() {
    return this.getAttribute('mobile-url-path') || "/mobile.html";
  }

  protected firstUpdated(_changedProperties: PropertyValues) {
    super.firstUpdated(_changedProperties);
    this.registry.listCommands().map(command => this.registry.resolve(command)).forEach(component => {
      // if (component instanceof BasePeerCommandComponent)
      // this.layout.appendChild(component);
      // else
      this.peerServices.push(component)
    });

  }

  private buildUrl() {
    const path = this.resolveMobilePath();
    const url = new URL(window.location.toString());
    url.pathname = path;
    url['searchParams'].set('remoteId', this.deviceId);
    return url.toString();
  }


  async handleRemoteStreamAdded(stream: MediaStream): Promise<void> {
    await this.handleRemoteTrackAdded(null, stream)
  }

  async handleRemoteTrackAdded(track, stream): Promise<void> {
    const video = document.createElement('video');
    const block = new ProportionalBlockElement();
    block.appendChild(video);
    console.log(stream.id);
    video.srcObject = stream;
    video.setAttribute('stream-id', stream.id);
    await animationFrameTrigger();
    this.layout.appendChild(block);
    await video.play();
  }

  handleRemoteData(message) {
    console.log(message);
    if (message.type === 'stream-settings') {
      const videoElement = this.findVideoElement(message.streamId);
      if (videoElement) {
        const block = (videoElement.parentElement as ProportionalBlockElement);
        block.ratio = message.settings.height / message.settings.width;
      }
    }
  }

  handleServerConnected(): void {
    console.log('connected to server');
  }

  private findVideoElement(streamId: string) {
    const videoElement = this.layout.querySelector(`video[stream-id="${streamId}"]`);
    console.log(videoElement);
    return videoElement;
  }

  protected async update(changedProperties: PropertyValues) {
    super.update(changedProperties);
    if (this.deviceId) {
      this.peerConnection.deviceId = this.deviceId;
    }
  }
}
