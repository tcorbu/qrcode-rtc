import 'reflect-metadata';
import './PresenterScreenElement';
import "../layout/ProportionalBlockElement";
import "../layout/FitRatiosLayoutElement";
import "../components/ShareButton";

import "../components/QrCodeElement";
import '../api/commands/FileSystemManager';
import '../layout/Layout';
import '../components/Footer';
import '../components/Menu';
import "../layout/Icon";
import "./PixabayImageSearchService";

import {PresenterScreenElement} from "./PresenterScreenElement";
import {assignScreenParameters} from "../labeling/assignScreenElementParameters";
import {container} from "tsyringe";
import {SSESignalingChannel} from "../api/signaling/SSESignalingChannel";
import "../api/connection/PresenterPeerConnection";

import '../service/main';

(async () => {
  container.registerInstance("presenterSignalingChannel", new SSESignalingChannel());
  const element = container.resolve(PresenterScreenElement);
  assignScreenParameters(element);
  document.body.appendChild(element);
  await element.updateComplete;
})();


