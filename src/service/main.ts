// const serviceWorkerPath = require('/sw.js').default;

if ('serviceWorker' in navigator) {
  window.addEventListener('load', async function () {
    // const serviceWorkerRegistrations = await navigator.serviceWorker.getRegistrations();
    // const p = serviceWorkerRegistrations.map(async registration => {
    //   console.log(registration);
    //   await registration.unregister();
    // });
    // await Promise.all(p);
    navigator.serviceWorker.register('/sw.js').then(function (registration) {
      // Registration was successful
      console.log('ServiceWorker registration successful with scope: ', registration.scope);
    }, function (err) {
      // registration failed :(
      console.log('ServiceWorker registration failed: ', err);
    });
  });
}
