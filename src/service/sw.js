var CACHE_NAME = 'webqrtc-v004';
var urlsToCache = [
  // '/',
  // '/landing.js',
  // '/presenter.js',
  // '/presenter.html',
  // '/mobile.js',
  // '/mobile.html',
  // '/components.js',
  // '/components.html',
  '/assets/fonts/Montserrat/Montserrat-Regular.woff2',
  '/assets/fonts/Open_Sans/OpenSans-Regular.woff2'
];

self.addEventListener('install', function (event) {
  // Perform install steps
  const removeOldCachesPromise = caches.keys().then((keyList) => {
    return Promise.all(keyList.map((key) => {
      if (key !== CACHE_NAME) {
        return caches.delete(key);
      }
    }));
  })

  const addUrlsToCachePromise = caches.open(CACHE_NAME)
    .then(function (cache) {
      console.log('Opened cache');

      return Promise.all(urlsToCache.map(url => {
        cache.addAll([url]).catch((e) => {
          console.error(`Unable add ${url} to the cache`);
        })
      }));
    }).catch((e) => {
      self.postMessage({error: 'Unable to open the cache', cause: e});
    });
  event.waitUntil(Promise.all([removeOldCachesPromise, addUrlsToCachePromise]));
});

self.addEventListener('fetch', function (event) {
  event.respondWith(
    caches.match(event.request)
      .then(function (response) {
        // Cache hit - return response
        if (response) {
          return response;
        }

        return fetch(event.request).then(
          function (response) {
            // Check if we received a valid response
            if (!response || response.status !== 200 || response.type !== 'basic') {
              return response;
            }

            // IMPORTANT: Clone the response. A response is a stream
            // and because we want the browser to consume the response
            // as well as the cache consuming the response, we need
            // to clone it so we have two streams.

            // var responseToCache = response.clone();
            //
            // caches.open(CACHE_NAME)
            //   .then(function (cache) {
            //     cache.put(event.request, responseToCache);
            //   });

            return response;
          }
        ).catch(e => {
          console.error('Fail to fetch');
        });
      })
  );
});
