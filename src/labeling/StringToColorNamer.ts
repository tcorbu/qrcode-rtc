import * as Color from 'color';
import crc24 from 'crc/crc24';
import * as ColorScheme from "color-scheme";

export class StringToColorNamer {
    static async computeForInput(input: string) {
        const color = StringToColorNamer.computeColorCode(input);
        return StringToColorNamer.computeForColor(color);
    }

    static async computeForColor(color: Color, schemeName = 'contrast') {
        const nameFromColor = StringToColorNamer.computeNameColor(color);
        const perceivedBrightness = StringToColorNamer.computePercevedBrightness(color);
        const foregroundColor = StringToColorNamer.computeForeground(perceivedBrightness, color);
        const backgroundColor = StringToColorNamer.computeBackground(perceivedBrightness, color);
        const scm = new ColorScheme;
        const scheme = scm.from_hex(color.hex().substring(1))
            .scheme(schemeName)
            .add_complement(false)
            .variation('pastel')
            .web_safe(true)
            .colors();
        const selected = Color.rgb('#' + scheme[2]);
        const selectedPercevedBrightness = StringToColorNamer.computePercevedBrightness(selected);
        const selectedForegroundColor = StringToColorNamer.computeForeground(selectedPercevedBrightness, selected);
        const selectedBackgroundColor = StringToColorNamer.computeBackground(selectedPercevedBrightness, selected);
        const hover = Color.rgb('#' + scheme[3]);
        const hoverPercevedBrightness = StringToColorNamer.computePercevedBrightness(hover);
        const hoverForegroundColor = StringToColorNamer.computeForeground(hoverPercevedBrightness, hover);
        const hoverBackgroundColor = StringToColorNamer.computeBackground(hoverPercevedBrightness, hover);
        return {
            color,
            nameFromColor,
            perceivedBrightness,
            foregroundColor,
            backgroundColor,
            selectedForegroundColor,
            selectedBackgroundColor,
            hoverForegroundColor,
            hoverBackgroundColor
        };
    }

    private static computeColorCode(uuid) {
        const hash = crc24(uuid);
        return Color.rgb('#' + hash.toString(16).padEnd(6, '0'));
    }

    public static computeNameColor(color): string {
        const path = require('../assets/colorNames.json');

        let colorNames = path.colors
        const value = parseInt('0x' + color.hex().substring(1));

        const pick = colorNames.reduce((acc, item, idx) =>
                Math.abs(item - value) < Math.abs(acc.value - value) ? {value: item, idx: idx} : acc
            , {idx: 0, value: 0});
        console.log(pick);
        return path.names[pick.idx];
        //
        // while (true) {
        //     const middle = colorNames.length / 2;
        //     const idx = middle - middle % 2;
        //     colorNames = value > colorNames[idx] ? colorNames.slice(idx) : colorNames.slice(0, idx);
        //     if (colorNames.length === 2) {
        //         return colorNames[1] as string;
        //     }
        //     if (colorNames.length === 0) {
        //         return 'Unknown';
        //     }
        // }
        // return namer(color.hex(), {pick: ['ntc']}).ntc[0].name;
    }

    private static computePercevedBrightness(color) {
        return (color.red() * 299 + color.green() * 587 + color.blue() * 114) / 1000
    }

    private static computeForeground(perceivedBrightness, color): Color {

        return perceivedBrightness > 127 ? new Color('#000') : color;
    }

    private static computeBackground(perceivedBrightness, color) {
        return perceivedBrightness > 127 ?   color : new Color('#FFF');
    }

}
