import {ScreenElement} from "../components/ScreenElement";
import {uuid} from "short-uuid";
import {StringToColorNamer} from "./StringToColorNamer";

export function setColorVariables(colors, element: HTMLElement) {

  element.style.setProperty('--background-color', colors.backgroundColor.hex());
  element.style.setProperty('--foreground-color', colors.foregroundColor.hex());
  element.style.setProperty('--selected-background-color', colors.selectedBackgroundColor.hex());
  element.style.setProperty('--selected-foreground-color', colors.selectedForegroundColor.hex());
  element.style.setProperty('--hover-background-color', colors.hoverBackgroundColor.hex());
  element.style.setProperty('--hover-foreground-color', colors.hoverForegroundColor.hex());

}

export async function applyColors(colors, element: ScreenElement) {
  setColorVariables(colors, element);
  element.name = colors.nameFromColor;
  const backgroundColor = colors.backgroundColor.hex();
  const foregroundColor = colors.foregroundColor.hex();
  const color = colors.color.hex();
  element.color = color;
  element.foregroundColor = foregroundColor;
  element.backgroundColor = backgroundColor;
  try {
    // const pixbay = new PixabayImageSearchService();
    // const result = await pixbay.findSearchQuery(colors.nameFromColor);
    // if (result.hits.length) {
    //     console.log(result);
    //     const index = Math.abs(result.hits.length * Math.random());
    //     const firstImage = result.hits[index].largeImageURL;
    //     element.backgroundImage = firstImage;
    // }
  } catch (e) {
    console.log(e);
  }
}

export function assignScreenParameters(element: ScreenElement) {
  (async () => {
    const id = new URL(window.location.href).searchParams.get('deviceId') || sessionStorage.getItem('deviceId') || uuid();
    element.setAttribute('deviceId', id);
    const colors = await StringToColorNamer.computeForInput(id);
    await applyColors(colors, element);
  })();
}
