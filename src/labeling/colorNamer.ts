const workerPath = '!!file-loader!./colorNamerWorker'

function getColorName(input: string) {
    return new Promise((res, rej) => {
        const worker = new Worker(workerPath);
        worker.onmessage = res;
        worker.postMessage(input);
    });
}
