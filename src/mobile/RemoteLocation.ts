import {css, customElement, html, query} from "lit-element";
import {LeafletMapComponent} from "../components/LeafetMap";
import {BasePeerCommandComponent} from "../api/commands/BasePeerCommandComponent";

@customElement('tc-remote-location')
export class RemoteLocation extends BasePeerCommandComponent {


    @query('tc-leaflet-map')
    private map: LeafletMapComponent;

    static get styles() {
        return css`tc-leaflet-map {
 
        }`;
    }

    protected render(): unknown {
        return html`
            <h2>Location</h2>
            <div><tc-toggle-button @active="${this.requestToggleNoSleep}" @inactive="${this.requestToggleNoSleep}"> Get Location </tc-toggle-button></div>
            <tc-leaflet-map></tc-leaflet-map>
        `;
    }

    async requestToggleNoSleep(): Promise<void> {
        const location = await this.send('getLocation')
        this.map.setView(location.latitude, location.longitude);

        this.map.addMarker(location.latitude, location.longitude);
    }

}