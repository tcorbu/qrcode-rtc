import "reflect-metadata";
import "./PushToSpeakComponent";
import "./RemoteMobileVideo";
import "./RemoteSettings";
import "./RemoteLocation";
import './SendFile';
import '../layout/Layout';
import '../components/Footer';
import '../components/ToggleButton';
import '../components/Menu';
import "../layout/Icon";
import "../components/Masonry";
import {assignScreenParameters} from "../labeling/assignScreenElementParameters";
import {MobileScreenElement} from "./MobileScreenElement";
import {container} from "tsyringe";
import {SSESignalingChannel} from "../api/signaling/SSESignalingChannel";
import '../commands/main';
import {RemoteMobileVideo} from "./RemoteMobileVideo";

export {MobileScreenElement} from './MobileScreenElement';

(async () => {
  const remoteId = new URL(window.location.href).searchParams.get('remoteId');
  container.registerInstance("mobileSignalingChannel", new SSESignalingChannel());
  const element = container.resolve(MobileScreenElement);
  element.remoteId = remoteId;
  assignScreenParameters(element);

  element.registry.registerPeerCommand('remote-video', RemoteMobileVideo);
  // element.registry.registerPeerCommand('location', RemoteLocation);
  document.body.querySelector('#main').appendChild(element);
  await element.updateComplete;
})();
