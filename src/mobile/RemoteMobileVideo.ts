import {css, customElement, html, property, PropertyValues, query} from 'lit-element';
import {PeerCommands} from "../api/commands/PeerCommands";
import {BasePeerCommandComponent} from "../api/commands/BasePeerCommandComponent";
import {BaseComponent} from "../components/BaseComponent";
import "../components/Dialog";

@customElement('tc-media-query-configuration')
class RemoteDeviceSettings extends BaseComponent {

  @property({attribute: false})
  public remoteDeviceCapabilities: MediaTrackSupportedConstraints;
  @property({attribute: false})
  public deviceMediaQuery: MediaStreamConstraints;
  @property()
  public capabilities: Capabilities;

  static get styles() {
    return css`
            label {
                margin: 6px 0;
                font-weight: bold;
                font-size: 18px;
                border-bottom: 1px solid var(--foreground-color);
                display: block;
            }
        `;
  }

  render() {
    const videoDevices = this.capabilities?.devices?.filter(item => item.kind === 'videoinput')?.map(item => {
      return {idx: item.deviceId, label: item.label, glyph: 'camera'}
    });
    const videoId = (this.deviceMediaQuery?.video as MediaTrackConstraints)?.deviceId;
    const selectedVideo = videoId ? [videoDevices.find(option => option.idx === videoId)] : [];

    const audioDevices = this.capabilities?.devices?.filter(item => item.kind === 'audioinput')?.map(item => {
      return {idx: item.deviceId, label: item.label, glyph: 'mic'}
    });
    const audioId = (this.deviceMediaQuery?.audio as MediaTrackConstraints)?.deviceId;
    const selectedAudio = audioId ? [audioDevices.find(option => option.idx === audioId)] : [];
    return html`<div>
                <label for="camera">Camera</label>
                <tc-list id="camera" .optionItems="${videoDevices}" .selection="${selectedVideo}"></tc-list>

                <label for="audio">Audio</label>
                <tc-list id="audio" .optionItems="${audioDevices}" .selection="${selectedAudio}"></tc-list>
        </div>`;
  }

}

interface Capabilities {
  devices: MediaDeviceInfo[];
  supportedConstraints: MediaTrackSupportedConstraints;
}

@customElement('tc-mobile-remote-video')
export class RemoteMobileVideo extends BasePeerCommandComponent {

  private _peerCommands: PeerCommands;
  @property({attribute: false})
  private remoteDeviceCapabilities: Capabilities;
  @property({attribute: false})
  private deviceMediaQuery: MediaStreamConstraints;
  private senders: Array<RTCRtpSender> = [];
  @property({type: Boolean})
  private settingsOpened: boolean = false;

  @query('video')
  public video: HTMLVideoElement;

  @query('tc-media-query-configuration')
  public settings: RemoteDeviceSettings;


  private tracks: MediaStreamTrack[];
  @property({attribute: false})
  private streaming: boolean;


  static get styles() {
    return css`

            .wrapper {
                display : flex;
                flex-flow: column nowrap;
                    background: var(--background-color);
                    color: var(--foreground-color);
            }

            video {
                flex: 1;
                min-height: 360px;
            }


        `;
  }

  render() {
    return html`
        <div class="wrapper">
            <h2> ${this.title}</h2>
            <video controls="controls"></video>
            <div class="controls">
            ${this.streaming ?
      html`<tc-button @click="${this.stop}" glyph="stream-cross">  Stop </tc-button>` :
      html`<tc-button @click="${this.requestDevice}"  glyph="stream">  Start </tc-button>`
    }
            <tc-button glyph="wrench2" @click="${(e) => {
      this.settingsOpened = true
    }}"> Settings </tc-button>
            </div>
        </div>
          ${this.settingsOpened ? html`
            <tc-dialog @close="${async (e) => {
      await this.applySettings();
    }}" title="Pick remotes Camera and Microphone">
            <tc-media-query-configuration .capabilities="${this.remoteDeviceCapabilities}" .devices="${this.remoteDeviceCapabilities.devices}" .deviceMediaQuery="${this.deviceMediaQuery}"></tc-media-query-configuration>
            </tc-dialog>
        ` : html`
        `}
    `
  }

  private async applySettings() {
    this.deviceMediaQuery = this.settings.deviceMediaQuery;
    if (this.streaming) {
      await this.stop();
      await this.requestDevice();
    }
    this.settingsOpened = false
  }

  protected async firstUpdated(_changedProperties: PropertyValues) {
    super.firstUpdated(_changedProperties);
    this.remoteDeviceCapabilities = await this.send({action: 'getDeviceCapabilities'});
    this.deviceMediaQuery = this.computeDefaultMediaQuery();
    await this.requestUpdate("remoteDeviceCapabilities");

  }

  async handleCommand(request): Promise<any> {
    if (request.action === 'getDeviceCapabilities') {
      return {
        devices: await navigator.mediaDevices.enumerateDevices(),
        supportedConstraints: await navigator.mediaDevices.getSupportedConstraints()
      };
    }
    if (request.action === 'requestDevice') {
      const result = [];
      const mediaStream = await navigator.mediaDevices.getUserMedia(request.query);
      for (const track of mediaStream.getTracks()) {
        const sender = await this.addTrack(track);
        this.senders.push(sender);
        result.push(track.id);
      }
      return result;
    }
    if (request.action === 'stop') {
      for (const sender of this.senders) {
        sender.track.stop();
        await this.removeTrack(sender);
      }
      this.senders = [];
    }
  }

  private async requestDevice() {
    this.setAttribute('disable', 'disable');
    const ids = await this.send({action: 'requestDevice', query: this.deviceMediaQuery});
    this.tracks = await this.getTracks(ids);
    this.streaming = true;
    await Promise.all(this.tracks.map(track => new Promise(res => track.addEventListener('unmute', res, {once: true}))));
    const mediaStream = new MediaStream(this.tracks);
    this.video.srcObject = mediaStream;
    if (this.video.paused) {
      await this.video.play();
    }
    this.removeAttribute('disable');
  }

  private computeDefaultMediaQuery(): MediaStreamConstraints {
    const video = this.remoteDeviceCapabilities.devices.find((device) => {
      return device.kind === 'videoinput';
    })
    const audio = this.remoteDeviceCapabilities.devices.find((device) => {
      return device.kind === 'audioinput';
    })
    return {
      audio: audio?.deviceId ? {
        deviceId: audio?.deviceId
      } : true,
      video: video?.deviceId ? {
        deviceId: video?.deviceId
      } : true
    }
  }

  public async stop() {
    if (this.tracks) {
      await this.send({action: 'stop', trackIds: this.tracks.map(track => track.id)})
    }
    this.streaming = false;
  }
}
