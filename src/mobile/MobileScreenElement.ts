import {css, customElement, html, property, PropertyValues, query} from "lit-element";

import {MediaDeviceManager} from "../media/MediaDeviceManager";
import {LoadingScreen} from "../components/LoadingScreen";
import {PeerCommands} from "../api/commands/PeerCommands";

import {ScreenElement} from "../components/ScreenElement";
import {StringToColorNamer} from "../labeling/StringToColorNamer";
import {inject, injectable} from "tsyringe";
import {MobilePeerConnection} from "../api/connection/MobilePeerConnection";
import {PeerCommandRegistry} from "../api/commands/PeerCommandRegistry";
import {Masonry} from "../components/Masonry";
import {BasePeerCommandComponent} from "../api/commands/BasePeerCommandComponent";
import {BasePeerCommand} from "../api/commands/BasePeerCommand";

@injectable()
@customElement('tc-mobile')
export class MobileScreenElement extends ScreenElement {
  public registry: PeerCommandRegistry;
  private peerServices: BasePeerCommand[] = [];

  static get styles() {
    return [css`

    `];
  }

  @property()
  public remoteId: string;
  public readonly type: string = 'mobile';

  @property({attribute: false})
  private remoteDevices: MediaDeviceInfo[];

  @property()
  private peerConnected: boolean;
  @property()
  private status: string;


  @query('tc-loading')
  loadingScreen: LoadingScreen;

  private peerCommands: PeerCommands;
  @query('tc-masonry')
  masonry: Masonry;

  render() {
    const connectedTemplate = html`
          <tc-send-file .peerCommands="${this.peerCommands}"></tc-send-file>
          <tc-push-to-speak .peerCommands="${this.peerCommands}" .mediaDeviceManager="${this.mediaDeviceManager}"></tc-push-to-speak>
          <tc-remote-settings .peerCommands="${this.peerCommands}"></tc-remote-settings>
        `;

    return html`
            <tc-masonry>
                ${this.peerConnected ? connectedTemplate : html`<tc-loading .status="${this.status}"></tc-loading>`}
           </tc-masonry>
    `;
  }

  constructor(
    @inject(MobilePeerConnection) peerConnection: MobilePeerConnection,
    @inject(MediaDeviceManager) private mediaDeviceManager: MediaDeviceManager,
  ) {
    super();
    this.peerConnection = peerConnection as MobilePeerConnection;
    this.registry = this.peerConnection.registry;
  }

  protected firstUpdated(_changedProperties: PropertyValues) {
    super.firstUpdated(_changedProperties);
    this.registry.listCommands().map(command => this.registry.resolve(command)).forEach(component => {
      if (component instanceof BasePeerCommandComponent)
        this.masonry.appendChild(component);
      else
        this.peerServices.push(component)
    });
  }

  protected async update(changedProperties: PropertyValues) {
    super.update(changedProperties);
    if (this.deviceId && changedProperties.has('deviceId')) {
      this.peerConnection.deviceId = this.deviceId;
      await this.start();
    }
  }

  private async start() {
    const screenName = await StringToColorNamer.computeForInput(this.remoteId);

    // const notification = this.layout.showNotification(`Connecting to  ${screenName.nameFromColor}`, 'Starting peer to peer connection', -1);
    await this.peerConnection.startSignaling(this.remoteId);
    this.peerConnection.retrievePeerCommands().then((res) => {
      this.peerCommands = res;
      this.status = 'connected';
      this.peerConnected = true;
    });
    this.status = 'Starting Connection';
  }
}
