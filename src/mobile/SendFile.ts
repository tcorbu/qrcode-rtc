import {customElement, html, LitElement, property, query} from 'lit-element';
import {FileTransferProgress} from "./FileTransferProgress";
import {PeerCommands} from "../api/commands/PeerCommands";

@customElement('tc-send-file')
export class SendFile extends LitElement {

    @query('input')
    private input: HTMLInputElement;

    @property({attribute: false})
    private peerCommands: PeerCommands;

    @query('#progresses')
    private progressesContainer: HTMLDivElement;

    private async handleFiles() {
        this.input.setAttribute('disabled', 'disabled');
        const files = this.input.files;
        try {
            if (files?.length) {
                const p = [];
                for (let i = 0; i < files.length; i++) {
                    const progress = new FileTransferProgress();
                    progress.file = files[i];
                    const progressUpdate = (current, total) => {
                        progress.seek = current;
                        progress.total = total;
                    };
                    this.progressesContainer.appendChild(progress);

                    p.push(this.peerCommands.sendFile(files[i], progressUpdate));
                }
                await Promise.all(p);
            }
        } finally {
            this.input.removeAttribute('disabled');
        }
    };

    public render() {
        return html`
        <div>
            <h2><tc-icon glyph="drawer-upload"></tc-icon>  Transfer files </h2>
            <input type="file" multiple @change="${this.handleFiles}">
        </div>
        <div id="progresses">
        
        </div>`
    }

}
