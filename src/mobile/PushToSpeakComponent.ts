import {css, customElement, html, LitElement, property} from 'lit-element';
import {MediaDeviceManager} from "../media/MediaDeviceManager";
import {TrackSenderMapping} from "../api/connection/PeerConnection";
import {PeerCommands} from "../api/commands/PeerCommands";

@customElement('tc-push-to-speak')
export class PushToSpeakComponent extends LitElement {

    static get styles() {
        return css`
        .blink {
          animation: blinker 1s linear infinite;
        }
        @keyframes blinker {
          0% {
            color: black;
          }
          50% {
            color: red;
          }
        }
        `;
    }

    @property({attribute: false})
    private mediaDeviceManager: MediaDeviceManager

    @property({attribute: false})
    private peerCommands: PeerCommands;
    private senders: TrackSenderMapping[];
    private track: MediaStreamTrack;

    render() {
        return html`
            <h2>Push to Speak</h2>
            <tc-toggle-button @active="${this.startCastingAudio}" @inactive="${this.stopCastingAudio}"> 
                <span slot="active"><tc-icon glyph="mic2" ></tc-icon> Speak </span>
                <span slot="inactive"><tc-icon class="blink" glyph="mic2"></tc-icon> Stop </span>
            </tc-toggle-button>
        `
    }

    async startCastingAudio() {
        await this.stopCastingAudio();
        const stream = await this.mediaDeviceManager.retriveDefaultAudioStream();
        this.track = stream.getTracks()[0];
        if (!this.senders) {
            this.senders = await this.peerCommands.addStream(stream);
        } else {
            await this.senders[0].sender.replaceTrack(this.track);
        }
    }

    async stopCastingAudio() {
        console.log('stop-casting');
        if (this.track) {
            this.track.stop();
            this.track = null;
        }
    }

}
