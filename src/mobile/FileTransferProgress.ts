import {css, customElement, html, LitElement, property} from 'lit-element';

@customElement('tc-file-transmit-progress')
export class FileTransferProgress extends LitElement {

    @property()
    public seek: number;
    @property()
    public total: number;
    @property({attribute: false})
    public file: File;

    static get styles() {
        return css`
             div { 
                display: flex;
                justify-content: space-between;
             }
    `;
    }

    render() {
        return html`
            <div>
                <span>${this.file.name}</span> <span @click="${this.requestCancel}">Cancel</span>
            </div>
            <progress id="file" max="${this.total}" value="${this.seek}"> 
                ${Math.round(this.seek / this.total) * 100} %
            </progress>
        `;
    }

    requestCancel() {
        this.dispatchEvent(new CustomEvent('cancel'))
    }

}
