import {BaseComponent} from "../components/BaseComponent";
import {customElement, html, property} from "lit-element";
import {PeerCommands} from "../api/commands/PeerCommands";

@customElement('tc-remote-settings')
export class RemoteSettings extends BaseComponent {

    private _peerCommands: PeerCommands;

    @property({attribute: false})
    get peerCommands(): PeerCommands {
        return this._peerCommands;
    };

    set peerCommands(value: PeerCommands) {
        const oldValue = this._peerCommands;
        this._peerCommands = value;
        this.requestUpdate('peerCommands', oldValue);
    }

    protected render(): unknown {
        return html`
            <h2>Settings</h2>
            <div><tc-toggle-button @active="${this.requestToggleNoSleep}" @inactive="${this.requestToggleNoSleep}"> Keep Screen On </tc-toggle-button></div>
            <div><tc-input></tc-input><tc-button glyph="play">Play video</tc-button></div>
            <div><input type="color" @change="${this.requestColorChange}"><tc-button>Set color</tc-button></div>
        `;
    }

    async requestToggleNoSleep(): Promise<void> {
        await this._peerCommands.requestToggleNoSleep();
    }

    async requestColorChange(e: Event): Promise<void> {
        await this._peerCommands.requestColorChange((e.target as HTMLInputElement).value);
    }

}