import {MediaDeviceManager} from "../media/MediaDeviceManager";
import {daTemplate} from "../template/TemplateHelper";
import {BaseComponent} from "../components/BaseComponent";
import {property} from "lit-element";

export class ListMediaDevicesElement extends BaseComponent {
    private mediaDeviceManager: MediaDeviceManager;

    @property({attribute: false})
    public devices = [];

    static get template() {
        return daTemplate(require('./ListMediaDevicesElement.pug'), require('../assets/grid.scss'));
    }

    constructor() {
        super();
        this.mediaDeviceManager = new MediaDeviceManager();
    }

    async connectedCallback() {
        super.connectedCallback();
        const videoSources = await this.mediaDeviceManager.retrieveVideoDevicesInfos();
        this.devices = videoSources;
    }
}
