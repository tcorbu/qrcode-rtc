import {ComputerVisionService} from './ComputerVisionService';

export class VideoPatternFinderElement extends HTMLElement {
    private service: ComputerVisionService;
    private video: HTMLVideoElement;
    private stream: MediaStream;
    private shadow: any;
    private findCanvas: HTMLCanvasElement;
    private trackerCanvas: HTMLCanvasElement;
    private markers: Array<any> = [];
    private workContext: CanvasRenderingContext2D | null;
    private resultCanvas: HTMLCanvasElement;
    private wrapperElement: HTMLDivElement;
    static template = require('./VideoPatternFinderTemplate.html');

    setService(service: ComputerVisionService) {
        this.service = service;
    }

    async connectedCallback() {
        console.log();
        this.shadow = this['attachShadow']({'mode': 'open'});
        this.shadow.innerHTML = VideoPatternFinderElement.template;

        this.trackerCanvas = this.shadow.querySelector('canvas.trackerCanvas');
        this.findCanvas = this.shadow.querySelector('canvas.findCanvas');
        this.resultCanvas = this.shadow.querySelector('canvas.resultCanvas');
        this.video = this.shadow.querySelector('video.userMedia');
        this.wrapperElement = this.shadow.querySelector('div');

        await this.activate();
        this.wrapperElement.addEventListener('click', (e) => {
            const box = this.video.getBoundingClientRect();
            this.captureFrame();
            const x = e.clientX - box.left, y = e.clientY - box.top;
            const imgData = this.retrieveTemplate(x, y);
            this.markers.push({id: +new Date, template: imgData, location: {x: x - 32, y: y - 32, w: 64, h: 64}});
        });

    }

    private retrieveTemplate(x: number, y: number) {

        return this.workContext.getImageData(x - 32, y - 32, 64, 64);

    }

    public captureFrame() {
        this.workContext.drawImage(this.video, 0, 0, this.video.width, this.video.height);
        return this.workContext.getImageData(0, 0, this.video.width, this.video.height);
    }

    public deactivate() {
        const tracks = this.stream.getTracks();
        for (const track of tracks) {
            track.stop();
        }
    }

    async find() {
        const source = this.captureFrame();
        if (this.markers.length) {
            const result = await this.service.find(source, this.markers);
            this.updateMarkers(result);
            this.drawFindings(result);
        }
        requestAnimationFrame(() => {
            this.find();
        })
    }

    private async activate() {
        this.stream = await navigator.mediaDevices.getUserMedia({video: true});
        this.video.srcObject = this.stream;
        await this.video.play();
        const box = this.video.getBoundingClientRect();
        this.findCanvas.width = box.width;
        this.findCanvas.height = box.height;
        this.resultCanvas.width = box.width;
        this.resultCanvas.height = box.height;
        this.video.width = box.width;
        this.video.height = box.height;
        this.workContext = this.findCanvas.getContext("2d");
        this.find();
    }

    private drawFindings(result: Array<any>) {
        const context = this.resultCanvas.getContext('2d');
        context.clearRect(0, 0, this.video.width, this.video.height);
        result.forEach((item) => {

            // context.putImageData(item.match.result, 0, 0);

            context.beginPath();
            context.arc(item.match.x, item.match.y, 32, 0, 2 * Math.PI);
            context.stroke();
            context.putImageData(item.template, item.match.x, item.match.y);
        })

    }

    private updateMarkers(result: Array<any>) {
        for (const item of result) {
            const marker = this.markers.find((marker) => item.id === marker.id);
            marker.template = this.retrieveTemplate(item.match.x, item.match.y);
        }
    }
}
