import {BaseComponent} from "../components/BaseComponent";
import {html} from "lit-html";
import {css, customElement} from "lit-element";

@customElement('tc-landing')
export class Landing extends BaseComponent {

  static get styles() {
    return css`

    `;
  }

  protected render(): unknown {
    return html`
      <tc-layout>
  <tc-section slot='header' style="flex: 1" >
      <div style="flex: 1; position: relative">
        <h1 >WebQRTC</h1>
        <p>A Peer To Peer toolbox </p>
          <tc-connected-dots ></tc-connected-dots>
    </div>

  </tc-section>
  <tc-section id="demo"  title="Try it now">
    <div class="presenter-demo-content">
    <slot name="demo"></slot>
    <p> Scan the code bellow to connect this Browser Tab with another device </p>
    <p> Do you need  a <a href="./scanner">scanner</a> ?</p>

    </div>
  </tc-section>
  <tc-section id="performance" title="Distributed and  performant">
    <p>This website uses the latest web technologies to provide the best possible experience, achieved 100 points on
      the Lighthouse test. WebRTC ensures a peer to peer connection, that requires a server just to establish the
      connection , after the connection is established, the two devices communicate directly, offering the most optimal
      transfer speed.</p>
  </tc-section>
  <tc-section id="opened" title="Opened">
    <p>The source code is available <a href="https://bitbucket.org/tcorbu/qrcode-rtc/src/master/">here</a>, you can
      submit an issue or a feature request on the <a
              href="https://bitbucket.org/tcorbu/qrcode-rtc/issues?status=new&status=open">issue
        tracker</a> page and Pull Requests, and reviews are welcomed over bitbucket <a
              href="https://bitbucket.org/tcorbu/qrcode-rtc"> repo </a> page </p>
    <p> The code is distributed under the Apache 2 licence, a copy of the licences used by this page are listed <a
            title="licences of used libraries" href="/landing.js.LICENSE.txt">here</a></p>
    <p>The background images are provided by <a href="https://pixabay.com/"> Pixabay</a></p>
  </tc-section>
  <tc-section id="privacy">
    <h2>Privacy </h2>
    <p>By default, the page doesn't use a single cookie, doesn't require a login and does no tracking.</p>
    <p>Some features of
      the site requires various permission to access for example the camera, the screen or the file system for obvious
      reasons in order to share the camera, the screen or files across devices, to
      change what is allowed, you can disable/enable these permissions over the settings panel </p>
    <tc-button>Open privacy settings</tc-button>
  </tc-section>
</tc-layout>
    `
  }

}
