import 'reflect-metadata';
import '../layout/Layout';
import "../components/Footer";
import "../components/ToastNotification";
import "../components/Section";
import {container} from "tsyringe";
import {PresenterScreenElement} from "../presenter/PresenterScreenElement";
import {assignScreenParameters} from "../labeling/assignScreenElementParameters";
import {RemoteMobileVideo} from "../mobile/RemoteMobileVideo";
import {PresenterRemoteLocation} from "../presenter/RemoteLocation";
import {SSESignalingChannel} from "../api/signaling/SSESignalingChannel";
import "../service/main"
import './Landing';
import "../components/ConnectedDots";


container.registerInstance("presenterSignalingChannel", new SSESignalingChannel());
const presenter = container.resolve(PresenterScreenElement);
assignScreenParameters(presenter);
sessionStorage.setItem('deviceId', presenter.deviceId);
// presenter.registry.registerPeerCommand('test', PresenterTestCommand);
presenter.registry.registerPeerCommand('remote-video', RemoteMobileVideo);
presenter.registry.registerPeerCommand('location', PresenterRemoteLocation);
presenter.setAttribute('slot', 'demo');
document.querySelector('tc-landing').appendChild(presenter);
