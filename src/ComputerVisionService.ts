import {Deferred} from "./async/Deferred";

export class ComputerVisionService {

    private worker: Worker;
    private pendingOperation: Deferred<any>;

    async initializeWorker() {
        return new Promise((res) => {
            this.worker = new Worker('/cv-worker.js');
            this.worker.addEventListener('message', this.handleMessage.bind(this));
            res(true);
        });
    }

    async find(frame, markers) {
        if (!this.pendingOperation) {
            this.pendingOperation = new Deferred();
            this.worker.postMessage({operation: 'findMarkers', frame: frame, markers: markers});

        }
        return this.pendingOperation.promise;
    }

    private handleMessage(event) {
        if (this.pendingOperation) {
            this.pendingOperation.resolve(event.data);
            this.pendingOperation = null;
        }
    }

}
