export class DelayedResolve<T> {
    private promise: Promise<T>;

    private constructor(timeout: number) {
        this.promise = new Promise((res) => {
            setTimeout(res, timeout)
        });
    }

    static async do(timeout) {
        await new DelayedResolve(timeout).promise;
    }

}