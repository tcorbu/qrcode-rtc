export class DelayedReject<T> {
    public promise: Promise<T>;

    constructor(timeout: number) {
        this.promise = new Promise((res, rej) => {
            setTimeout(() => {
                rej({cause: 'timeout'});
            }, timeout)
        });
    }
}