import {Deferred} from "./Deferred";
import {raceAganinstTimeout} from "./raceAgainstTimeout";
import {uuid} from "short-uuid";


export interface PendingMessage<REQ, RES> {
    id: string;
    request: REQ;
    promise?: Deferred<RES>
}

export interface IncomingMessage<RES> {
    id: string;
    response: RES;
}

class PendingMessageTimeoutError extends Error {

}

export class SyncroniousRequestResponseMessaging<REQ, RES> {

    private pendingMessages: Array<PendingMessage<REQ, RES>> = [];

    constructor(private sendCallback) {

    }

    response(message: any) {
        if (message.id) {
            const idx = this.pendingMessages.findIndex(pendingMessage => pendingMessage.id === message.id);
            if (idx > -1) {
                delete message.id;
                this.pendingMessages[idx].promise.resolve(message);
                this.pendingMessages.splice(idx, 1);
            }
        }
    };

    async request(request: REQ, timeout: number = 1000): Promise<RES> {
        const pendingMessage = this.prepareMessage(request);
        this.pendingMessages.push(pendingMessage);
        this.sendCallback(this.serializeMessage(pendingMessage, request));
        let result;
        try {
            result = await raceAganinstTimeout(pendingMessage.promise, timeout);
        } catch (e) {
            if (e.cause === 'timeout')
                throw new PendingMessageTimeoutError();
        }
        return result;
    }

    private serializeMessage(pendingMessage, request: REQ) {
        return Object.assign({id: pendingMessage.id}, request);
    }

    private prepareMessage(request: REQ): PendingMessage<REQ, RES> {
        const response = new Deferred<RES>();
        return {
            id: uuid(),
            request: request,
            promise: response
        };

    }
}
