import {Deferred} from "./Deferred";

export function animationFrameTrigger() {
    const d = new Deferred();
    window.requestAnimationFrame(d.resolve);
    return d.promise;
}