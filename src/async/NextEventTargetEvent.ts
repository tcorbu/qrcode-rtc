import {Deferred} from "./Deferred";

export function resolveDeferredOnNextEventTargetEvent<T>(deferred: Deferred<T>, eventTarget: EventTarget, eventType) {
    const listener = (e) => {
        deferred.resolve(e as T);
        eventTarget.removeEventListener(eventType, listener);
    };
    eventTarget.addEventListener(eventType, listener);
}

export function nextEventTargetEvent<T>(eventTarget: EventTarget, eventType):Promise <T> {
    const d = new Deferred<T>();
    const listener = (e) => {
        d.resolve(e as T);
        eventTarget.removeEventListener(eventType, listener);
    };
    eventTarget.addEventListener(eventType, listener);
    return d.promise;
}

