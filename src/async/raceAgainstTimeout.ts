import {DelayedReject} from "./DelayedReject";
import {Deferred} from "./Deferred";

export function raceAganinstTimeout<T>(deferred: Deferred<T>, timeout: number): Promise<T> {
    return Promise.race([deferred.promise, new DelayedReject<T>(timeout).promise]);
}
