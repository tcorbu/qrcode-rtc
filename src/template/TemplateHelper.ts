export function daTemplate(template: Function, ...styles: string[]) {
    const htmlTemplateElement = document.createElement('template');
    const templateCompileRenderParams = {style: styles.join('\n')};
    htmlTemplateElement.innerHTML = template(templateCompileRenderParams);
    return htmlTemplateElement;
}