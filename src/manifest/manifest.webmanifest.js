module.exports = JSON.stringify({
  "short_name": "WebQRTC",
  "name": "WebQRTC: Peer2Peer Toolset",
  "description": 'A lightweight peer to peer connection tools set',
  "icons": [
    {
      "src": "/assets/images/logo_192.png",
      "type": "image/png",
      "sizes": "192x192"
    },
    {
      "src": "/assets/images/logo_512.png",
      "type": "image/png",
      "sizes": "512x512"
    }
  ],
  "start_url": "/",
  "background_color": "#3367D6",
  "display": "standalone",
  "scope": "/",
  "theme_color": "#3367D6",
  "shortcuts": [
    // {
    //   "name": "How's weather today?",
    //   "short_name": "Today",
    //   "description": "View weather information for today",
    //   "url": "/today?source=pwa",
    //   "icons": [{"src": "/images/today.png", "sizes": "192x192"}]
    // },
    // {
    //   "name": "How's weather tomorrow?",
    //   "short_name": "Tomorrow",
    //   "description": "View weather information for tomorrow",
    //   "url": "/tomorrow?source=pwa",
    //   "icons": [{"src": "/images/tomorrow.png", "sizes": "192x192"}]
    // }
  ]
});
