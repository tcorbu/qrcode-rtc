import {BaseComponent} from "./BaseComponent";
import {property, PropertyValues} from "lit-element";
import {PeerConnection} from "../api/connection/PeerConnection";

export class ScreenElement extends BaseComponent {

    @property()
    backgroundImage: string;

    @property()
    backgroundColor: string;

    @property()
    foregroundColor: string;

    @property()
    name: string;

    @property()
    color: string;

    @property()
    deviceId: string;


    protected update(changedProperties: PropertyValues) {
        super.update(changedProperties);
        if (changedProperties.get('deviceId')) {
            this.peerConnection.deviceId = changedProperties.get('deviceId') as string;
        }
    }

    protected peerConnection: PeerConnection;
}