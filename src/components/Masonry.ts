import {css, customElement, html} from "lit-element";
import {BaseComponent} from "./BaseComponent";

@customElement('tc-masonry')
export class Masonry extends BaseComponent {
    static get styles() {
        return css`
            .grid-layout {
              display: grid;
              grid-template-columns: repeat(4, minmax( 1fr, auto));
              grid-auto-flow: dense;
              gap: 10px;
            }
            
            slot::slotted(.span-2)  {
                grid-column-end: span 2;
                grid-row-end: span 2;
            }
            
            slot::slotted(.span-3) {
                grid-column-end: span 3;
                grid-row-end: span 4;
            }
        `;


    }

    render() {
        return html`
            <div class="grid-layout">
                <slot></slot>
            </div>
        `
    }
}