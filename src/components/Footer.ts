import {BaseComponent} from "./BaseComponent";
import {css, customElement, html} from "lit-element";

@customElement('tc-footer')
export class Footer extends BaseComponent {

  static get styles() {
    return css`
            :host {
                display : grid;
                justify-content: space-around;

            }
        `;
  }

  render() {
    return html`
            <span> Made in Munich</span>
<!--            <span> Background Images offered by <a href="https://pixabay.com/" title="Pixabay"><tc-icon glyph="pixbay-logo"></tc-icon></a></span>-->
        `;
  }
}
