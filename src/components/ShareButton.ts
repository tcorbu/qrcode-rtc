import {BaseComponent} from "./BaseComponent";
import {customElement, html, property, PropertyValues} from "lit-element";

@customElement('tc-share-button')
export class ShareButton extends BaseComponent implements ShareData {

    @property()
    text: string;

    @property()
    title: string;

    @property()
    url: string;

    render() {
        return html`<tc-icon glyph="share"></tc-icon>`;
    }

    protected firstUpdated(_changedProperties: PropertyValues) {
        super.firstUpdated(_changedProperties);
        this.onclick = this.handleClick.bind(this);
    }

    async handleClick() {
        try {
            await navigator.share(this);
        } catch (err) {

        }
    }

}