import {BaseComponent} from "./BaseComponent";
import {css, customElement, html, property, TemplateResult} from "lit-element";
import {OptionItem} from "./OptionItem";
import {classMap} from "lit-html/directives/class-map";

@customElement('tc-selectable-option-item')
export class SelectableOptionItem extends BaseComponent {

    @property({attribute: false})
    public optionItem: OptionItem;
    @property({type: Boolean})
    private selected: boolean;

    static get styles() {
        return css`
               .wrapper {
                     background: var(--background-color);
                     color: var(--foreground-color);
                     display: flex;
                     padding: 3px;
                     align-items : center;
                     cursor: pointer;
               }
               
               .wrapper:hover {
                    background: var(--hover-background-color);
                    color: var(--hover-foreground-color);
               }
               
               .selected {
                    --background-color: var(--selected-background-color);  
                    --foreground-color: var(--selected-foreground-color);
                    
                }
               
               .label {
                    flex: 1;
               }
               tc-icon {
                    margin: 3px;
               }
        `;
    }

    protected render(): unknown {
        const classes = {selected: this.selected};
        return html`

        <div class="wrapper ${classMap(classes)}">
            ${this.optionItem?.glyph ? html`<tc-icon glyph="${this.optionItem?.glyph}"></tc-icon>` : html``}
            <span class="label"> ${this.optionItem?.label} </span>
            </div>
        `
    }

}

@customElement('tc-list')
export class List extends BaseComponent {
    private _search: string;
    private _optionsResolver: (search) => Generator<OptionItem>;

    static get styles() {
        return css`
            :host {
                display: flex;
                flex-flow: column nowrap;
                overflow-y: auto;
                flex : 1;
                
            }
        `;
    }

    @property({type: Boolean})
    public singleSelection: boolean = true;
    @property({type: Boolean})
    public optional: boolean = false;

    @property()
    set optionsResolver(resolver: (search) => Generator<OptionItem>) {
        this.optionItemsGenerator = resolver(this.search);
        this._optionsResolver = resolver;
    };

    get optionsResolver(): (search) => Generator<OptionItem> {
        return this._optionsResolver;
    };

    @property()
    set search(search: string) {
        this.optionItemsGenerator = this.optionsResolver ? this.optionsResolver(search) : (function* () {
            yield 'NO_RESULT';
        });
        this._search = search;
    }

    get search() {
        return this._search;
    }


    @property()
    buffer: number = 20;
    private _generator: any;


    private set optionItemsGenerator(generator) {
        this._generator = generator;
        this.optionItems = [];
        this.loadNextPage();
    }

    private get optionItemsGenerator() {
        return this._generator;
    }

    private lastItem: IteratorResult<OptionItem>;
    @property()
    public selection: OptionItem[] = [];

    @property()
    private optionItems: OptionItem[] = [];


    protected render(): TemplateResult {
        return html`${
            this.optionItems?.map(item => html`<tc-selectable-option-item @click="${this.toggleSelection}" ?selected="${!!~this.findIndexOf(item)}" .optionItem="${item}"></tc-selectable-option-item>`)
        } ${!this.lastItem || this.lastItem?.done ? html`` : html`<span @click="${this.loadNextPage}"><tc-icon  glyph="chevron-down"></tc-icon> Load More</span>`}`;
    }

    private async loadNextPage(): Promise<void> {
        if (!this.optionItemsGenerator) {
            return;
        }
        for (let i = 0; i < this.buffer; i++) {
            this.lastItem = this.optionItemsGenerator.next();
            const data = this.lastItem.value;
            this.optionItems.push(data);
            if (this.lastItem.done) {
                break;
            }
        }
        await this.requestUpdate('optionItems')
    }


    toggleSelection(e) {
        const item = (e.target as SelectableOptionItem).optionItem;
        if (this.singleSelection) {

            const removedItem = this.selection.pop();
            if (!this.optional || removedItem?.idx !== item.idx) {
                this.selection.push(item);
            }
        } else {
            const index = this.findIndexOf(item);
            if (!!~index) {
                this.selection.splice(index, 1);
            } else {
                this.selection.push(item);
            }
        }
        this.requestUpdate('selection');
        this.dispatchEvent(new CustomEvent('selection-change', {detail: this.selection}));
    }

    private findIndexOf(item: OptionItem): number {
        return this.selection.findIndex((selected) => item.idx === selected.idx);
    }
}