import {css, customElement, html, query} from "lit-element";
import {LeafletMapComponent} from "../components/LeafetMap";
import {BasePeerCommandComponent} from "../api/commands/BasePeerCommandComponent";

@customElement('tc-serial-peer-command')
export class SerialPeerCommand extends BasePeerCommandComponent {


  static get styles() {
    return css`tc-leaflet-map {

        }`;
  }

  protected render(): unknown {
    return html`
            <h2>Serial Peer Command</h2>
        `;
  }


}
