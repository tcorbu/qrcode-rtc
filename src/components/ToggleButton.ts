import {css, customElement, property, PropertyValues, query} from "lit-element";
import {Button} from "./Button";

@customElement('tc-toggle-button')
export class ToggleButton extends Button {
    @property({type: Boolean})
    private active: boolean = false;

    static get styles(): any {
        return [Button.styles, css`.active{
                background: var(--selected-background-color);
                border: 1px solid var(--selected-foreground-color);
                color: var(--selected-foreground-color);
        }`];
    }

    @query('div')
    wrapper: HTMLDivElement;

    render() {
        return super.render();
    };

    protected firstUpdated(_changedProperties: PropertyValues) {
        this.applyClass();
        this.wrapper.addEventListener('click', () => {
            this.active = !this.active;
            this.applyClass();
            console.log('stuff')
            const event = new CustomEvent(this.active ? 'active' : 'inactive');
            this.dispatchEvent(event);
        });
    }


    private applyClass() {
        this.wrapper.classList.remove('active', 'inactive');
        this.wrapper.classList.add(this.active ? 'active' : 'inactive')
    }
}