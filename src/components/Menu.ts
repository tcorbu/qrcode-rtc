import {BaseComponent} from "./BaseComponent";
import {css, customElement, html} from "lit-element";

@customElement('tc-menu')
export class Menu extends BaseComponent {

    static get styles() {
        return css`
            :host {
                background-color : var(--background-color);
                color : var(--foreground-color);
                display: flex;
                flex:1;
            }
            a {
                color : var(--foreground-color);
            }
            a:hover {
                background-color : var(--selected-background-color);
                color : var(--selected-foreground-color);
            }
        `
    }

    protected render() {
        return html`
            <ul>
                <li><a href="/presenter.html">Presenter</a></li>
                <li><a href="/" > <tc-icon glyph="home"></tc-icon>Home</a>  </li>  
                <li> 
                    <div>Tools</div>
                    <ul>
                        <li><a href="/scanner.html" > <tc-icon glyph="qrcode"></tc-icon>QRCode Scanner</a>  </li>
                        <li><a href="/generator.html" > <tc-icon glyph="qrcode"></tc-icon>QRCode Generator</a>  </li>
                        <li><a href="/screen-recorder.html" > <tc-icon glyph="monitor"></tc-icon>Screen Recorder</a>  </li>
                        <li><a href="/image-converter.html" > <tc-icon glyph="picture"></tc-icon>Image Converter</a>  </li>
                    </ul>  
                </li> 
            </ul>
        `;
    }

}
