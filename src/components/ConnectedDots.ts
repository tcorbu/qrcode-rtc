import {BaseComponent} from "./BaseComponent";
import {css, customElement, html, PropertyValues, query} from "lit-element";

interface Star {
  x: number;
  y: number;
  vx: number;
  vy: number;
  fx: number;
  fy: number;
  radius: number;
}

@customElement('tc-connected-dots')
export class ConnectedDots extends BaseComponent {
  private ctx: CanvasRenderingContext2D;
  private stars: any[];
  private FPS: number;
  private x: number;
  private mouse: { x: number; y: number };

  static get styles() {


    return css`

        div {
           width:100%;
           height: 100%;
        }
           :host, canvas {
            display: block;
            position: absolute;
            top: 0px;

            left: 0px;
            width: 100%;
            height: 100%;
            background : var(--background-color);
            z-index: -1;
    }
        }`;
  }

  public render(): unknown {
    return html`
            <div>

                <canvas></canvas>
                          <slot></slot>
            </div>`;
  }

  @query('canvas')
  public canvas: HTMLCanvasElement;


  protected firstUpdated(_changedProperties: PropertyValues) {
    super.firstUpdated(_changedProperties);
    const box = this.canvas.getBoundingClientRect();
    this.canvas.width = box.width;
    this.canvas.height = box.height;

    this.ctx = this.canvas.getContext('2d');


    this.stars = [] // Array that contains the stars
    this.FPS = 60 // Frames per second
    this.x = 50 // Number of stars
    this.mouse = {
      x: 0,
      y: 0
    };  // mouse location

// Push stars to array

    for (var i = 0; i < this.x; i++) {
      this.stars.push({
        x: Math.random() * this.canvas.width,
        y: Math.random() * this.canvas.height,
        radius: Math.random() * 0.5+0.1,
        vx: Math.floor(Math.random() * 10) - 5,
        vy: Math.floor(Math.random() * 10) - 5
      });
    }

// Draw the scene


    this.tick();
  }


  draw() {

    const color = getComputedStyle(this).getPropertyValue('--foreground-color');
    const background = getComputedStyle(this).getPropertyValue('--background-color');
    this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);

    this.ctx.globalCompositeOperation = "lighter";

    for (var i = 0, x = this.stars.length; i < x; i++) {
      var s = this.stars[i];

      this.ctx.fillStyle = color;
      this.ctx.beginPath();
      this.ctx.arc(s.x, s.y, s.radius, 0, 2 * Math.PI);
      this.ctx.fill();
      this.ctx.fillStyle = background;
      this.ctx.stroke();
    }

    this.ctx.beginPath();
    for (var i = 0, x = this.stars.length; i < x; i++) {
      var starI = this.stars[i];
      this.ctx.moveTo(starI.x, starI.y);
      if (this.distance(this.mouse, starI) < 75) this.ctx.lineTo(this.mouse.x, this.mouse.y);
      for (var j = 0, x = this.stars.length; j < x; j++) {
        var starII = this.stars[j];
        if (this.distance(starI, starII) < 75) {
          //ctx.globalAlpha = (1 / 150 * distance(starI, starII).toFixed(1));
          this.ctx.lineTo(starII.x, starII.y);
        }
      }
    }
    this.ctx.lineWidth = 0.05;
    this.ctx.strokeStyle = color;
    this.ctx.stroke();
  }

  distance(point1, point2) {
    var xs = 0;
    var ys = 0;

    xs = point2.x - point1.x;
    xs = xs * xs;

    ys = point2.y - point1.y;
    ys = ys * ys;

    return Math.sqrt(xs + ys);
  }

// updateDots star locations

  moveWithAntigravity(dt = 9, o: Star[]) {  // "o" refers to Array of objects we are moving
    for (let o1 of o) {  // Zero-out accumulator of forces for each object
      o1.fx = 0;
      o1.fy = 0;
    }
    for (let [i, o1] of o.entries()) {  // For each pair of objects...
      for (let [j, o2] of o.entries()) {
        if (i < j) {  // To not do same pair twice
          let dx = o2.x - o1.x;  // Compute distance between centers of objects
          let dy = o2.y - o1.y;
          let r = Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2));
          if (r < 1) {  // To avoid division by 0
            r = 1;
          }
          // Compute force for this pair; k = 1000
          let f = (1000 * o1.radius * o2.radius) / Math.pow(r, 2);
          let fx = f * dx / r;  // Break it down into components
          let fy = f * dy / r;
          o1.fx += fx;  // Accumulate for first object
          o1.fy += fy;
          o2.fx -= fx;  // And for second object in opposite direction
          o2.fy -= fy;
        }
      }
    }
    for (let o1 of o) {  // for each object update...
      let ax = (o1.fx / (o1.radius * 2 * Math.PI)) * -1;  // ...acceleration
      let ay = (o1.fy / (o1.radius * 2 * Math.PI)) * -1;

      o1.vx += ax * dt;  // ...speed
      o1.vy += ay * dt;

      o1.x += o1.vx * dt;  // ...position
      o1.y += o1.vy * dt;

      if (o1.x < 0 || o1.x > this.canvas.width) o1.vx = -o1.vx;
      if (o1.y < 0 || o1.y > this.canvas.height) o1.vy = -o1.vy;
    }
  }

  updateDots() {
    for (var i = 0, x = this.stars.length; i < x; i++) {
      var s = this.stars[i];

      s.x += s.vx / this.FPS;
      s.y += s.vy / this.FPS;

      if (s.x < 0 || s.x > this.canvas.width) s.vx = -s.vx;
      if (s.y < 0 || s.y > this.canvas.height) s.vy = -s.vy;
    }
  }

  // canvas.addEventListener('mousemove', function (e) {
  //     mouse.x = e.clientX;
  //     mouse.y = e.clientY;
  // });

// updateDots and draw

  tick() {
    const box = this.canvas.getBoundingClientRect();
    this.canvas.width = box.width;
    this.canvas.height = box.height;

    this.draw();
    this.moveWithAntigravity(0.050, this.stars);
    requestAnimationFrame(this.tick.bind(this));
  }


}
