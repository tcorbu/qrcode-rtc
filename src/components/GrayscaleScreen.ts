import {css, customElement, html, LitElement, property, PropertyValues} from 'lit-element';


@customElement('tc-grayscale')
export class GrayscaleScreen extends LitElement {

    @property()
    public status: string;
    @property()
    public foregroundColor: string = '#fff';
    @property()
    public backgroundColor: string = '#000';

    static get styles() {
        return css`
        
        :host {
            display: flex;
            position: fixed;
            top: 1px;
            bottom: 1px;
            left: 1px;
            right: 1px;
            margin: 0;
            z-index: 100;
            transition : backdrop-filter 0.5s;
            backdrop-filter: blur(0px) grayscale(0);
       
        }
     `
    }

    render() {
        return html`<slot></slot>`
    }

    connectedCallback() {
        // @ts-ignore
        super.connectedCallback();


    }

    protected firstUpdated(_changedProperties: PropertyValues) {
        super.firstUpdated(_changedProperties);
        setTimeout(() => {
            // @ts-ignore
            this.style.backdropFilter = 'blur(4px)  grayscale(100%)';
        });

    }

    async close() {
        const p = Promise.race([
            new Promise((res) => {
                this.ontransitionend = res;
            }),
            new Promise(res => setTimeout(res, 500))
        ]);
        // @ts-ignore
        this.style.backdropFilter = 'blur(0px) grayscale(0%)';
        await p;
        this.parentElement.removeChild(this);
    }

}
