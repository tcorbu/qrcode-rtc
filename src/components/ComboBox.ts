import {BaseComponent} from "./BaseComponent";
import {customElement, html, property, TemplateResult} from "lit-element";


@customElement('tc-combo-box')
export class ComboBox extends BaseComponent {

    @property()
    private glyph: string;


    protected render(): TemplateResult {

        return html`
            <tc-icon glyph="${this.glyph}"></tc-icon>
            <tc-input></tc-input>
            <div>
                <tc-list></tc-list>
            </div>
        `;

    }

}