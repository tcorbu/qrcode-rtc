import {css, customElement, html} from "lit-element";
import {BaseComponent} from "./BaseComponent";

@customElement('tc-section')
export class Section extends BaseComponent {
  static get styles() {
    return css`

    section {
     color : var(--foreground-color);
     display : flex;
     min-height: 360px;
     flex-flow: column nowrap;
     padding: 12px;
    }
    div {
      display : flex;
      flex-flow: column nowrap;
      flex: 1;
    }
    `;


  }

  render() {
    return html`
            <section>
                ${this.title?html`<h2>${this.title}</h2>`:html``}
                <div>
                    <slot></slot>
                </div>
            </section>
        `
  }
}
