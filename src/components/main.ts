import "reflect-metadata"
import {BaseComponent} from "./BaseComponent";
import {css, customElement, html, property} from "lit-element";
import "../layout/Icon";
import {Masonry} from "./Masonry";
import "../mobile/PushToSpeakComponent";
import "../mobile/RemoteMobileVideo";
import "../mobile/RemoteSettings";
import "../mobile/RemoteLocation";
import '../mobile/SendFile';
import '../layout/Layout';
import crc24 from 'crc/crc24';
import './components.html.handlebars';
import {container} from "tsyringe";
import {LocalSignalingChannel} from "../api/signaling/LocalSignalingChannel";
import {PresenterScreenElement} from "../presenter/PresenterScreenElement";
import {MobileScreenElement} from "../mobile/MobileScreenElement";
import {assignScreenParameters, setColorVariables} from "../labeling/assignScreenElementParameters";
import "./QrCodeElement";
import "./Footer";
import "./ToggleButton";
import "./Menu";
import './Button';
import {BasePeerCommandComponent} from "../api/commands/BasePeerCommandComponent";
import {until} from "lit-html/directives/until";
import {RemoteLocation} from "../mobile/RemoteLocation";
import {PresenterRemoteLocation} from "../presenter/RemoteLocation";
import {RemoteMobileVideo} from "../mobile/RemoteMobileVideo";
import {List} from "./List";
import {uuid} from "short-uuid";
import * as Color from "color";
import {StringToColorNamer} from "../labeling/StringToColorNamer";

import '../service/main';
import {Section} from "./Section";
import {ConnectedDots} from "./ConnectedDots";

(async () => {
    const color = Color.rgb('#FFF')
    const colors = await StringToColorNamer.computeForColor(color);
    setColorVariables(colors, document.body);
})()

@customElement('tc-icon-demo')
class IconDemo extends BaseComponent {
    @property()
    glyph: string;

    static get styles() {
        return css`
                div {
                    text-align: center;
                }
                tc-icon {
                    display: inline-block;
                    width: 48px;
                    height: 48px;
                }
        `;


    }


    constructor(glyph: string) {
        super();
        this.glyph = glyph;
    }

    render() {
        return html`
            <div>
                <tc-icon glyph="${this.glyph}"></tc-icon>
                <div>${this.glyph}</div>
            </div>
        `
    }
}

const section = new Section();
section.title = 'Icons';
const masonry = new Masonry();
section.appendChild(masonry)
document.body.appendChild(section);
// for (const icon of icons) {
//     masonry.appendChild(new IconDemo(icon.id));
// }


function setupLocalSignalingChannel() {
    const mobileSignalingChannel = new LocalSignalingChannel();
    const presenterSignalingChannel = new LocalSignalingChannel();

    mobileSignalingChannel.peerSignalingChannel = presenterSignalingChannel;
    presenterSignalingChannel.peerSignalingChannel = mobileSignalingChannel;
    container.registerInstance("presenterSignalingChannel", presenterSignalingChannel);
    container.registerInstance("mobileSignalingChannel", mobileSignalingChannel);
}

@customElement('tc-peer-command-mobile-test')
class MobileTestCommand extends BasePeerCommandComponent {
    protected render() {
        return html`${until(this.great().then(res => html`hello back to you`), html`sending some command`)}`;
    }

    private great() {
        return this.send({type: 'hello'});
    }
}

@customElement('tc-peer-command-presenter-test')
class PresenterTestCommand extends BasePeerCommandComponent {
    @property()
    public lastMessage: any;

    protected render() {
        return html`${this.lastMessage}`;
    }

    handleCommand(data: any): Promise<any> {
        this.lastMessage = JSON.stringify(data);
        return Promise.resolve('stuff');
    }
}

(() => {


    setupLocalSignalingChannel();

    const section = new Section();
    section.title = 'Peer Connection';
    const presenter = container.resolve(PresenterScreenElement);
    assignScreenParameters(presenter);
    presenter.registry.registerPeerCommand('test', PresenterTestCommand);
    presenter.registry.registerPeerCommand('remote-video', RemoteMobileVideo);
    presenter.registry.registerPeerCommand('location', PresenterRemoteLocation);


    const mobile = container.resolve(MobileScreenElement);
    mobile.registry.registerPeerCommand('test', MobileTestCommand);
    mobile.registry.registerPeerCommand('remote-video', RemoteMobileVideo);
    mobile.registry.registerPeerCommand('location', RemoteLocation);

    mobile.remoteId = presenter.deviceId;
    assignScreenParameters(mobile);
    section.appendChild(mobile);


    section.appendChild(presenter);
    document.body.appendChild(section);
})();


(() => {
    const section = new Section();
    section.title = 'List';
    const input = document.createElement('input');
    section.appendChild(input);
    const list = new List();
    input.onchange = () => {
        list.search = input.value;
    }

    list.optionsResolver = function* (search) {
        let i = 0;
        while (true) {
            const s = uuid();
            const hash = crc24(s);
            const color = Color.rgb('#' + hash.toString(16).padEnd(6, '0'));
            yield {label: StringToColorNamer.computeNameColor(color), idx: i++, glyph: 'bus'};
        }
    }
    section.appendChild(list)
    document.body.appendChild(section);
})();

(() => {
    const section = new Section();
    section.title = 'Connected Dots';
    const connectedDots = new ConnectedDots();
    const newChild = document.createElement('h1');
    newChild.innerHTML = 'KJASHDKJAHSDLKJASHDLKAJSHDLKAJSHDD asddkljhaslakdjfhsdlkfjh'
    connectedDots.appendChild(newChild);
    section.appendChild(connectedDots)
    document.body.appendChild(section);
})()
