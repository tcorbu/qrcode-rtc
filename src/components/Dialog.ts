import {BaseComponent} from "./BaseComponent";
import {css, customElement, html, query} from "lit-element";
import {Icon} from "../layout/Icon";

@customElement('tc-dialog')
export class Dialog extends BaseComponent {
    @query('.content')
    private dialogContent: HTMLDivElement;
    @query('.close')
    private closeIcon: Icon;

    static get styles() {
        return css`
            .wrapper {
             display: grid;
              grid-template-areas: "tl t tr"
                                   "l c r"
                                   "bl b br";
              grid-gap: 16px;
              align-items: flex-center;
              justify-content: center;
              flex: 1; 
            }
            .content {
                display : flex:
                align-items: center;
                background-color : var(--background-color);
                color : var(--foreground-color);
                padding: 12px;
                border: 1px solid var(--foreground-color);
                grid-area: c;
                
            }
            
            header {
                display: flex;
                flex-flow: row nowrap;
                width: 100%;
                align-items: center;
                margin-bottom: 24px;
            }
            
            h2 {
                flex: 1;
            }
            
            .close {
                margin: 3px;
            }
            
            
        `;
    }

    protected render() {
        return html`<tc-grayscale  @click="${this.close}">
                <div class="wrapper">
                    <div class="content">
                        <header><h2>${this.title}</h2><tc-icon  class="close" glyph="cross" @click="${this.close}"></tc-icon> </header>
                        <slot></slot>
                    </div>
                </div>
            </tc-grayscale>`;
    }

    close(ev) {
        if (ev.composedPath().includes(this.closeIcon) || !ev.composedPath().includes(this.dialogContent))
            this.dispatchEvent(new CustomEvent('close'));

    }

    open(element: HTMLElement) {
        element.appendChild(this);
    };

}