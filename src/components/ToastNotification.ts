import {BaseComponent} from "./BaseComponent";
import {css, customElement, html, property, PropertyValues} from "lit-element";
import {Deferred} from "../async/Deferred";

@customElement('tc-notification')
export class ToastNotification extends BaseComponent {
    @property()
    image: string;
    @property()
    content: string;
    @property()
    action: Function;
    @property()
    autoCloseTimeout = -1;
    private timerId: number;
    @property()
    private autoCloseProgress: number;
    private result: Deferred<any> = new Deferred<any>();

    static get styles() {
        return css`
            :host {
                  display: grid;
                  grid-template-columns: auto auto auto 32px;
                  grid-template-rows: 64px auto 32px;
                  column-gap: 8px;
                  grid-template-areas: 
                    "header header header close"
                    "main main main main"
                    "footer footer footer footer"
                    ;
                   border : 1px solid var(--foreground-color);
                   color : var(--foreground-color);
                   background-color : var(--background-color);
                   border: 1px solid var(--foreground-color);
                   padding: 12px 12px 0 12px;
                   transition : transform 0.5s, opacity 0.5s;
                   transform-origin: top;
            }
            h3, tc-icon {
                margin: 0px;
                grid-area: header;
        
            }        
            tc-icon {
                grid-area: close;
            }
            img, .content {
                grid-area: main;
            }
            .footer {
                border-bottom: 3px solid var(--foreground-color);
                transition : width .16s;
                grid-area: footer;
                
            }
        `;

    }

    render() {
        return html`
            
            <h3>${this.title}</h3>
            <tc-icon glyph="cross" @click="${this.close}"></tc-icon>
            ${this.image ? html`<img src="${this.image}" />` : ``}
            <p class="content">
                <slot>
                    ${this.content}
                <slot>
            </p>
            <div class="footer" style="width: ${100 - 100 * this.autoCloseProgress}% ">
                <slot name="footer"></slot>
            </div>
        `
    }

    protected firstUpdated(_changedProperties: PropertyValues) {
        super.firstUpdated(_changedProperties);
        if (this.autoCloseTimeout > -1) {
            let timerStarted = Date.now();
            let pause = false;
            const tick = async () => {
                if (pause) {
                    return;
                }
                if (timerStarted + this.autoCloseTimeout < Date.now()) {
                    await this.close();
                } else {
                    this.autoCloseProgress = (Date.now() - timerStarted) / this.autoCloseTimeout;
                    requestAnimationFrame(tick.bind(this));
                }
            }
            tick();

            this.onmouseover = () => {
                pause = true;
            }

            this.onmouseleave = () => {
                timerStarted = Date.now();
                pause = false;
                tick();
            }

            this.ontouchstart = () => {
                pause = true;
            }

            this.ontouchend = () => {
                timerStarted = Date.now();
                pause = false;
                tick();
            }
        }


    }

    private async close() {
        await this.fadeOut();
        this.parentElement.removeChild(this);
        this.result.resolve('closed');
    }

    getResult(): Promise<any> {
        return this.result.promise;
    }

    private async fadeOut() {
        this.style.transform = 'scale(1, 0)';
        this.style.opacity = '0';
        return new Promise((res) => this.ontransitionend = res);
    }
}