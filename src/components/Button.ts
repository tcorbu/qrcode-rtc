import {BaseComponent} from "./BaseComponent";
import {css, customElement, html, property} from "lit-element";

@customElement('tc-button')
export class Button extends BaseComponent {
    @property()
    public glyph: string;
    @property()
    private label: string;

    static get styles() {
        return css`
            div {
                display : inline-flex;
                padding: 6px 6px 6px 0px;
                background: var(--background-color);
                color: var(--foreground-color);
                border: 1px solid var(--foreground-color);
                align-items: center;
                cursor: pointer;
                
                
                  position: relative;
                  overflow: hidden;
                  transition: background 400ms;
            
       
                  font-family: 'Roboto', sans-serif;
                  font-size: 16px;
        
         
                  border-radius: 2px;
                  box-shadow: 0 0 2px rgba(0, 0, 0, 0.3);
                  
                
            }
            
            div:hover {
                background: var(--hover-background-color);
                border: 1px solid var(--hover-foreground-color);
                color: var(--hover-foreground-color);
            }
            
           span, tc-icon {
               padding-left : 6px; 
            }
        `;
    }

    protected render(): unknown {
        return html`
            <div>
                ${this.glyph ? html`<tc-icon glyph="${this.glyph}"></tc-icon>` : html``}
                <span>
                    <slot>
                        ${this.label}
                    </slot>
                </span>
            </div>
        `;
    }

}