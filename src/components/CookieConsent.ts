import {BaseComponent} from "./BaseComponent";
import {customElement, html} from "lit-element";

@customElement('tc-cookie-consent')
export class CookieConsent extends BaseComponent {

    protected render(): unknown {
        return html`
            This site uses some cookies, you can turn the marketing and tracking off if you like.
        `
    }

}