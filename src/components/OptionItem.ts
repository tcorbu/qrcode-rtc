export interface OptionItem {
    label: string;
    idx?: number;
    glyph?: string;
}