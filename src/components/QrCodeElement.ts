import {toString} from "qrcode";
import {BaseComponent} from "../components/BaseComponent";
import {unsafeSVG} from "lit-html/directives/unsafe-svg";
import {customElement, html, property} from "lit-element";
import {until} from "lit-html/directives/until";
@customElement('tc-qrcode')
export class QrCodeElement extends BaseComponent {

    @property()
    private data: string;
    @property()
    private foregroundColor: string;
    @property()
    private backgroundColor: string;

    protected render() {
        return html`
        <div class="qrcode">
            ${until(this.renderQrCode(), html`<span>Loading...</span>`)}
        </div>
        `
    }

    async renderQrCode() {
        const qr = await toString(this.data, {
            color: {
                dark: this.foregroundColor,
                light: this.backgroundColor
            },
            type: 'svg'
        });
        return unsafeSVG(qr);
    }

}
