export class DocumentTitle {
    private title: string;

    showTemporaryTitle(title: string, timeout = 5000) {
        if (!this.title) {
            this.title = document.title;
        }
        document.title = title;
        setTimeout(this.restoreTitle.bind(this), timeout);

    }

    restoreTitle() {
        if (this.title) {
            document.title = this.title;
            this.title = null;
        }
    }


}