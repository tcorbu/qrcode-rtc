import {injectable} from "tsyringe";

export interface MediaStreamEntry {

  input?: Promise<MediaStream>;
  deviceInfo: MediaDeviceInfo;
}
@injectable()
export class MediaDeviceManager {

  private static streams: Array<MediaStreamEntry> = [];

  async retrieveDefaultVideoStream() {
    await this.retrieveVideoDevicesInfos();
    let stream;
    try {
      stream = await this.retrieveChromeScreencastStream();
    } catch (e) {

    }
    return stream = await navigator.mediaDevices.getUserMedia({audio: true, video: true});
  }

  async retriveDefaultAudioStream() {
    return navigator.mediaDevices.getUserMedia({
      audio: {
        noiseSuppression: {ideal: false},
        echoCancellation: {ideal: true}
      }, video: false
    });
  }


  public async retrieveVideoDevicesInfos() {
    const deviceInfos = await this.retrieveDeviceInfos();
    return deviceInfos.filter((device) => device.kind === 'videoinput');
  }

  public async retrieveDeviceInfos(): Promise<Array<MediaDeviceInfo>> {
    return await navigator.mediaDevices.enumerateDevices();
  }

  public async retrieveChromeScreencastStream() {

    console.log(screen.height * window.devicePixelRatio);

    const constraints = {

      audio: false, // mandatory.
      video: {
        'mandatory': {
          chromeMediaSource: 'screen',
          minHeight: screen.height * window.devicePixelRatio,
          minWidth: screen.width * window.devicePixelRatio
        },
      }
    };
    let result: MediaStream;
    try {
      result = await navigator.mediaDevices.getUserMedia(constraints as MediaStreamConstraints);
    } catch (e) {
      console.error(e)
    }
    return result;
  }

  retrieveStreamForDevice(deviceInfo: MediaDeviceInfo): Promise<MediaStream> {
    const constraints: MediaStreamConstraints = {};
    if (deviceInfo.kind === 'videoinput') {
      constraints.video = {
        deviceId: deviceInfo.deviceId,
      }
    }
    return navigator.mediaDevices.getUserMedia(constraints);
  }

  disposeStream(stream: MediaStream) {
    stream.getTracks().forEach((track) => {
      track.stop();
      stream.removeTrack(track);
    });

  }

  isChromeScreenCastCapable() {
    // TODO : implement it
    return true;
  }


  requestStream(deviceInfo: MediaDeviceInfo) {
    let opened = this.ensureDeviceInfoEntry(deviceInfo);

    if (opened && opened.input) {
      return opened;
    } else {
      const input = this.retrieveStreamForDevice(deviceInfo);

      opened.input = input;
      return opened;
    }

  }

  private ensureDeviceInfoEntry(deviceInfo: MediaDeviceInfo) {
    if (!deviceInfo) {
      return
    }
    let opened = MediaDeviceManager.streams.find((item) =>
      item.deviceInfo.deviceId === deviceInfo.deviceId
    );
    if (!opened) {

      opened = {deviceInfo};
      MediaDeviceManager.streams.push(opened);
    }
    return opened;
  }

  findExistingStream(deviceInfo: MediaDeviceInfo) {
    return MediaDeviceManager.streams
      .find(item => item.deviceInfo.deviceId === deviceInfo.deviceId);
  }

  async removeStream(deviceInfo: MediaDeviceInfo): Promise<void> {
    const findPredicate = item => item.deviceInfo.deviceId === deviceInfo.deviceId;
    const idx = MediaDeviceManager.streams.findIndex(findPredicate);
    if (idx > -1) {
      const entry = MediaDeviceManager.streams[idx];
      const stream = await entry.input;
      this.disposeStream(stream);

      MediaDeviceManager.streams.splice(idx, 1);
    }
  }

}
