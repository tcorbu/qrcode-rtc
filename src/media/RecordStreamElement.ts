import {Deferred} from "../async/Deferred";
import {MediaDownloads} from "./MediaDownloadsManager";
import {daTemplate} from "../template/TemplateHelper";
import {BaseComponent} from "../components/BaseComponent";

export class RecordStreamElement extends BaseComponent {

    public static get template() {
        return daTemplate(require('./RecordStreamTemplate.pug'))
    }

    public static get properties() {
        return {
            stream: {
                type: MediaStream,
                observer: "setStream"
            }
        };
    }

    private shadow: ShadowRoot;
    private recordButton: HTMLButtonElement;
    private stopButton: HTMLButtonElement;
    private stream: MediaStream;
    private recorder: MediaRecorder;
    private data: any[];
    private startTimestamp: number;
    private downloadManager: MediaDownloads;

    constructor() {
        super();
        this.downloadManager = new MediaDownloads();
    }

    connectedCallback() {
        super.connectedCallback();
        this.recordButton = this.shadowRoot.querySelector('#record') as HTMLButtonElement;
        this.stopButton = this.shadowRoot.querySelector('#stop') as HTMLButtonElement;

        const startRecordingListener = async () => {
            this.recordButton.removeEventListener('click', startRecordingListener);
            this.recordButton.setAttribute('disable', 'true');
            this.stopButton.setAttribute('disable', 'false');
            await this.startRecording();
            this.recordButton.addEventListener('click', startRecordingListener);
        };
        this.recordButton.addEventListener('click', startRecordingListener);
    }

    setStream(stream: MediaStream) {

        this.stream = stream;
        if (!this.stream) {
            return;
        }
        if (this.stream.active) {
            this.handleActivatedStream();
        }
        this.stream.addEventListener('active', () => {
            console.log('active');
            this.handleActivatedStream();
        });
        this.stream.addEventListener('inactive', () => {
            console.log('inactive');
            this.handleInactivatedStream();
        });
        this.recorder = new MediaRecorder(this.stream);
        this.recorder.addEventListener('dataavailable', this.recorderDataAvailableListener.bind(this));
    }

    private recorderDataAvailableListener = (event: MediaRecorderDataAvailableEvent) => {
        this.data.push(event.data);
        this.downloadRecording(this.data);
    };

    async startRecording() {
        if (!this.recorder) {
            return;
        }
        this.data = [];


        const stop = this.stopPromise();
        const error = this.errorPromise();
        this.startTimestamp = +new Date;
        this.recorder.start();
        this.enableStopButton();
        await Promise.race([stop, error]);
        this.recorder.stop();
        this.handleActivatedStream();
    }


    private async stopPromise(): Promise<void> {
        const def = new Deferred<void>();
        const stopButtonListener = (event) => {
            this.stopButton.removeEventListener('click', stopButtonListener);
            def.resolve(event);
        };
        this.stopButton.addEventListener('click', stopButtonListener);
        return def.promise;
    }

    downloadRecording(recordedChunks) {
        const download = {data: recordedChunks, name: (+new Date + '_download'), type: 'video/webm'};
        this.downloadManager.addDownload(download)
    }

    private handleActivatedStream() {
        this.recordButton.removeAttribute('disabled');
        this.stopButton.setAttribute('disabled', 'true');
    }

    private handleInactivatedStream() {
        // TODO : implement it
    }

    private errorPromise() {
        const def = new Deferred<void>();
        const listener = (event) => {
            this.recorder.removeEventListener('error', listener);
            def.resolve(event);
        };
        this.recorder.addEventListener('error', listener);
        return def.promise;
    }

    private enableStopButton() {
        this.stopButton.removeAttribute('disabled');
        this.recordButton.setAttribute('disabled', 'true');
    }

    async reset() {
        this.recorder = null;
        this.stream = null;
    }

    disconnectedCallback() {
        this.recorder = null;
        this.stream = null;

    }
}