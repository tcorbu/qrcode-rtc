export interface MediaDownload {
  name: string;
  data: any;
  type: string;
}

class MediaDownloadsManager implements MediaDownloadsContract {

  constructor(private intial: Array<MediaDownload>) {

  }

  addDownload(download: MediaDownload) {

  }

  removeDownload(download: MediaDownload) {

  }

}

interface MediaDownloadsContract {

  addDownload(download: MediaDownload);

  removeDownload(download: MediaDownload);
}

export class MediaDownloads implements MediaDownloadsContract {

  static instance: MediaDownloadsContract = new MediaDownloadsManager([]);

  addDownload(download: MediaDownload) {
    MediaDownloads.instance.addDownload(download);
  }

  removeDownload(download: MediaDownload) {
    MediaDownloads.instance.removeDownload(download);
  }

}

