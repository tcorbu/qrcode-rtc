export class VideoDownloadElement extends HTMLElement {
    private shadow: ShadowRoot;

    constructor(recordedChunks: Array<number>, name: string = (+new Date + '_download')) {
        super();
        this.shadow = this.attachShadow({mode: "closed"});
        this.createDownloadLink(recordedChunks, name);
        this.createDisposeLink();
    }

    private createDownloadLink(recordedChunks: Array<number>, name: string) {
        const a = document.createElement('a');
        // @ts-ignore
        let recordedBlob = new Blob(recordedChunks, {type: "video/webm"});
        // recording.src = URL.createObjectURL(recordedBlob);
        a.href = URL.createObjectURL(recordedBlob);
        a.download = "RecordedVideo.webm";
        a.innerText = name;
        this.shadow.appendChild(a);
    }

    private createDisposeLink() {
        const a = document.createElement('a');
        a.addEventListener('click', () => {
            this.parentElement.removeChild(this);
        });
        this.shadow.appendChild(a);
    }
}
