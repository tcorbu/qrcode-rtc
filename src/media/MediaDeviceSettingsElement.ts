import {BaseComponent} from "../components/BaseComponent";

export class MediaDeviceSettingsElement extends BaseComponent {

    static get template() {
        const template = require('./MediaDeviceSettingsElement.pug');
        const htmlTemplateElement = document.createElement('template');
        htmlTemplateElement.innerHTML = template();
        return htmlTemplateElement;
    }

}
