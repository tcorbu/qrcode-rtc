import {RecordStreamElement} from "./RecordStreamElement";
import {VideoDownloadElement} from "./VideoDownloadElement";
import {MediaDeviceSettingsElement} from "./MediaDeviceSettingsElement";

export function declareMediaStreamElements() {
    window.customElements.define('tc-record-stream', RecordStreamElement);
    window.customElements.define('tc-video-download', VideoDownloadElement);
    window.customElements.define('tc-media-device-settings', MediaDeviceSettingsElement);
}