FROM node:14-alpine

WORKDIR /app
COPY . /app
# Install deps
RUN  npm ci && npm run build && rm -rf /data/dist && cp -r dist /data
